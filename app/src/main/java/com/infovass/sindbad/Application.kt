package com.infovass.sindbad

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import com.downloader.PRDownloader
import com.google.firebase.FirebaseApp
import com.google.gson.Gson
import com.infovass.sindbad.View.main.MainActivity
import com.infovass.sindbad.View.SplashActivity
import com.infovass.sindbad.data.model.MessageResultsItem
import com.onesignal.OneSignal

class Application : Application() {

    val context = this

    override fun onCreate() {
        super.onCreate()
        PRDownloader.initialize(applicationContext)
       /* // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .setNotificationReceivedHandler {
                //  val notificationData = it.payload.additionalData
                // Log.i("Notification data", "$notificationData")

            }
            .setNotificationOpenedHandler {
                val notificationData = it.notification.payload.additionalData
                if (notificationData != null) {

                    val gson = Gson()
                    val message =
                        gson.fromJson(notificationData.toString(), MessageResultsItem::class.java)

                    val intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("fromNotification", true)
                    intent.putExtra("MessageResultsItem", message)
                    startActivity(intent)

                } else {
                    val intent = Intent(this, SplashActivity::class.java)
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
            }
            .init()*/
    }

}