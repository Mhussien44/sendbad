package com.infovass.sindbad

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}