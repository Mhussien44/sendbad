package com.infovass.sindbad

import com.infovass.sindbad.data.model.DocumentResultsItem


interface ItemClickListener {
    fun onClick(position: Int, document: DocumentResultsItem)
}