package com.infovass.sindbad.Utils

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.Button

class CustomButton : Button {
    internal var context: Context? = null

    constructor(context: Context) : super(context) {
        this.context = context
        setFont()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.context = context
        setFont()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this.context = context
        setFont()
    }

    private fun setFont() {
        val font = Typeface.createFromAsset(context?.assets, "fonts/GE SS Two Medium_1.otf")
        setTypeface(font, Typeface.NORMAL)
    }
}
