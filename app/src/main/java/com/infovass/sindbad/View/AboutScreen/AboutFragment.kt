package com.infovass.sindbad.View.AboutScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import kotlinx.android.synthetic.main.about_fragment.*
import kotlinx.android.synthetic.main.app_bar_main2.*


class AboutFragment : Fragment() {

    companion object {
        fun newInstance() = AboutFragment()
    }

    private lateinit var viewModel: AboutViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.about_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.about_us)

        val progress = CustomProgressBar.showProgressBar(context!!)
        progress.show()
        viewModel = ViewModelProviders.of(this).get(AboutViewModel::class.java)
        viewModel.getAboutUs().observe(this, Observer { about ->
            progress.dismiss()
            if (about != null) {
                if (about.checkErrors == false) {
                    aboutText.text = about.results
                }
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })
    }

}
