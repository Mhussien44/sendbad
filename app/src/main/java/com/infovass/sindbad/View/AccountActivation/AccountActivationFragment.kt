package com.infovass.sindbad.View.AccountActivation

import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.*
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.View.main.MainFragment
import com.infovass.sindbad.data.model.User
import kotlinx.android.synthetic.main.account_activation_fragment.*
import kotlinx.android.synthetic.main.app_bar_main2.*

class AccountActivationFragment : Fragment() {

    companion object {
        fun newInstance(accessToken: String?, phone: String) = AccountActivationFragment().apply {
            arguments = Bundle().apply {
                putString("accessToken", accessToken)
                putString("phone", phone)
            }
        }
    }

    private lateinit var viewModel: AccountActivationViewModel
    private var timer : CountDownTimer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.account_activation_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.activeAccount)
        viewModel = ViewModelProviders.of(this).get(AccountActivationViewModel::class.java)

        val progress = CustomProgressBar.showProgressBar(context!!)
        accountActivationBut.setOnClickListener {
            val accessToken = arguments?.getString("accessToken")?.toString()
            if (isValidCode()) {
                progress.show()
                val code = getCode()
                viewModel.verifyPhone(code, accessToken).observe(this, Observer { user ->
                    progress.dismiss()
                    if (user != null) {
                        if (user.accessToken != null && user.success != null) {
                            makeToast(context!!, resources.getString(R.string.success))
                            storeUserData(user)
                            goToNextScreen()
                        } else
                            makeToast(context!!, user.message.toString())
                    } else
                        makeToast(context!!, resources.getString(R.string.errors))
                })
            }
        }
        moveCursorToNextEditText()
        counter()
    }

    private fun goToNextScreen() {
        fragmentManager?.inTransaction {
            replace(R.id.mainContainer, MainFragment.newInstance())
        }
    }

    private fun storeUserData(user: User?) {
        SharedPreferenceUtil(context!!).apply {
            putString(USERID, user?.success?.id.toString())
            putString(TOKEN, user?.accessToken.toString())
            putString(USERNAME, user?.success?.name.toString())
            putString(USERPHONE, user?.success?.phone.toString())
        }
    }

    private fun getCode(): String {
        val code = "${code1.text}${code2.text}${code3.text}${code4.text}${code5.text}${code6.text}"
        return code
    }

    private fun isValidCode(): Boolean {
        return if (getCode() != "") {
            if (getCode().length == 6)
                true
            else {
                makeToast(context!!, resources.getString(R.string.enterVaidCode))
                false
            }
        } else {
            makeToast(context!!, resources.getString(R.string.enterVaidCode))
            false
        }

    }

    private fun moveCursorToNextEditText() {
        code1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code2.requestFocus()
                }
            }

        })

        code2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code3.requestFocus()
                }
            }

        })

        code3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code4.requestFocus()
                }
            }

        })

        code4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code5.requestFocus()
                }
            }

        })

        code5.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code6.requestFocus()
                }
            }

        })

        code6.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    accountActivationBut.performClick()
                }
            }

        })
    }

    private fun setupTimer() {

    }

    private fun counter() {
        timerText.visibility = View.VISIBLE
        resendActivationCode.visibility = View.GONE
         timer = object : CountDownTimer(120 * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val seconds = (millisUntilFinished / 1000).toInt() % 60
                val minutes = (millisUntilFinished / (1000 * 60) % 60).toInt()
                timerText.text = "${resources.getString(R.string.resendActivationCode)}    ${String.format(
                    "%d:%d",
                    minutes,
                    seconds
                )}"
            }

            override fun onFinish() {
                timerText.visibility = View.GONE
                resendActivationCode.visibility = View.VISIBLE
                resendActivationCode()
            }
        }
        timer?.start()
    }

    private fun resendActivationCode() {
        val progress = CustomProgressBar.showProgressBar(context!!)
        val phone = arguments?.getString("phone")
        resendActivationCode.setOnClickListener {
            progress.show()
            viewModel.resendActivationCode(phone.toString()).observe(this, Observer { result ->
                progress.dismiss()
                counter()
                if (result != null) {
                    if (result.checkErrors != null) {
                        makeToast(context!!, resources.getString(R.string.codeSend))
                    } else
                        makeToast(context!!, result.message.toString())
                } else
                    makeToast(context!!, resources.getString(R.string.errors))
            })
        }
    }

    override fun onStop() {
        super.onStop()
        timer?.cancel()
    }
}

