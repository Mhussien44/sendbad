package com.infovass.sindbad.View.AccountActivation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.data.model.ActivationCode
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.repository.AppRepository
import com.infovass.sindbad.data.repository.AuthRepository

class AccountActivationViewModel (application: Application) : AndroidViewModel(application){


    fun verifyPhone(code: String, accessToken: String?) : MutableLiveData<User> {
        val authRepository = AppRepository("$accessToken")
        return authRepository.verifyPhone(code)
    }

    fun resendActivationCode(phone: String) : MutableLiveData<ActivationCode> {
        val authRepository = AuthRepository("")
        return authRepository.resendActivationCode(phone)
    }


}
