package com.infovass.sindbad.View.ChangePassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil
import com.infovass.sindbad.Utils.ValidationUtils
import kotlinx.android.synthetic.main.change_pass_fragment.*
import kotlinx.android.synthetic.main.reset_password_fragment.changePass
import kotlinx.android.synthetic.main.reset_password_fragment.changePassConfirm

class ChangePassFragment : Fragment() {

    companion object {
        fun newInstance() = ChangePassFragment()
    }

    private lateinit var viewModel: ChangePassViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.change_pass_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChangePassViewModel::class.java)
        val progress = CustomProgressBar.showProgressBar(context!!)

        changePassBut.setOnClickListener {
            if (isValidInputs()) {
                progress.show()
                viewModel.changePassword(getUserInputs()).observe(this, Observer { result ->
                    progress.dismiss()
                    if (result != null) {
                        if (result.checkErrors == false && result.result != null) {
                            SnackAndToastUtil.makeToast(context!!, result.result.toString())
                            activity?.onBackPressed()
                        } else
                            SnackAndToastUtil.makeToast(context!!, result.message.toString())
                    } else
                        SnackAndToastUtil.makeToast(context!!, resources.getString(R.string.errors))
                })
            }

        }

    }


    private fun getUserInputs(): HashMap<String, String> {
        val map = HashMap<String, String>()
        map["password"] = changePass.text.toString()
        map["password_confirmation"] = changePassConfirm.text.toString()
        map["old_password"] = changePassOld.text.toString()
        return map
    }

    private fun isValidInputs(): Boolean {
        return when {
            !ValidationUtils.isValidPassword(changePassOld.text.toString()) -> {
                changePassOld.error = resources.getString(R.string.correct_pass)
                false
            }
            !ValidationUtils.isValidPassword(changePass.text.toString()) -> {
                changePass.error = resources.getString(R.string.correct_pass)
                false
            }
            !ValidationUtils.isValidPassword(changePassConfirm.text.toString()) -> {
                changePassConfirm.error = resources.getString(R.string.correct_pass)
                false
            }
            changePassConfirm.text.toString() != changePass.text.toString() -> {
                SnackAndToastUtil.makeToast(context!!, resources.getString(R.string.mismatch_pass))
                false
            }
            else -> true
        }
    }

}
