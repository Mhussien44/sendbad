package com.infovass.sindbad.View.ChangePassword

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.ChangePassResponse
import com.infovass.sindbad.data.repository.AuthRepository

class ChangePassViewModel(application: Application) : AndroidViewModel(application) {

    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")
    private val authRepository = AuthRepository(token.toString())

    fun changePassword(map: HashMap<String, String>): MutableLiveData<ChangePassResponse> {
        return authRepository.changePassword(map)
    }

}
