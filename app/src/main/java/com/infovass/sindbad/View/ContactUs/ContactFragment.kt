package com.infovass.sindbad.View.ContactUs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.USERNAME
import com.infovass.sindbad.Utils.USERPHONE
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.contact_fragment.*


class ContactFragment : Fragment() {

    companion object {
        fun newInstance() = ContactFragment()
    }

    private lateinit var viewModel: ContactViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.contact_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.contact)
        initUserInfo()
        viewModel = ViewModelProviders.of(this).get(ContactViewModel::class.java)
        val progress = CustomProgressBar.showProgressBar(context!!)

        sendContact.setOnClickListener {
            if (isValidContactData()) {
                progress.show()
                viewModel.sendContectMessage(getContactInfo()).observe(this, Observer { result ->
                    progress.dismiss()
                    if (result != null) {
                        if (result.checkErrors == false) {
                            makeToast(context!!, resources.getString(R.string.sendSuccess))
                            activity?.onBackPressed()
                        } else
                            makeToast(context!!, result.message.toString())
                    } else
                        makeToast(context!!, resources.getString(R.string.errors))
                })
            }
        }


    }

    private fun isValidContactData(): Boolean {
       // val title = `contactTitleُ`.text.toString()
        val description = contactMessage.text.toString()
        return if (description.isEmpty()) {
            contactMessage.error = resources.getString(R.string.message_requird)
            false
        } else true
    }

    private fun initUserInfo() {
        val userName = SharedPreferenceUtil(context!!).getString(USERNAME, "")
        val userPhone = SharedPreferenceUtil(context!!).getString(USERPHONE, "")
        contactName.setText(userName)
        contactPhone.setText(userPhone)
    }

    private fun getContactInfo(): HashMap<String, String> {
        val title = `contactTitleُ`.text.toString()
        val description = contactMessage.text.toString()
        val map = HashMap<String, String>()
        map["title"] = title
        map["text"] = description
        return map
    }
}
