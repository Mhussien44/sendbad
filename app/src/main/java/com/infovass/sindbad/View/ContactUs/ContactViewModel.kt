package com.infovass.sindbad.View.ContactUs

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.Contact
import com.infovass.sindbad.data.repository.MessagesRepository

class ContactViewModel(application: Application) : AndroidViewModel(application) {

    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")
    private val repository = MessagesRepository(token.toString())

    fun sendContectMessage(map: HashMap<String, String>): MutableLiveData<Contact> {
        return repository.sendContactRequest(map)
    }
}
