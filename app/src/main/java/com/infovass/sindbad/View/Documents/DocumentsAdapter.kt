package com.infovass.sindbad.View.Documents

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.ItemClickListener
import com.infovass.sindbad.R
import com.infovass.sindbad.data.model.DocumentResultsItem
import kotlinx.android.synthetic.main.document_item.view.*

class DocumentsAdapter constructor(
    context: Context,
    private val itemClickListener: ItemClickListener
) :
    RecyclerView.Adapter<DocumentsAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    private var documents: List<DocumentResultsItem>? = null
    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.document_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = documents?.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (documents == null)
            0
        else
            documents!!.size
    }

    fun setDocuments(documents: List<DocumentResultsItem?>?){
        this.documents = documents as List<DocumentResultsItem>?
    }
    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(data: DocumentResultsItem?, position: Int) {
            itemView.docFileName.text = data?.name
            itemView.docDownload.setOnClickListener {
                if (data != null) {
                    data.actionType = "download"
                    itemClickListener.onClick(position, data)
                }
            }
            itemView.docPreview.setOnClickListener {
                if (data != null) {
                    data.actionType = "preview"
                    itemClickListener.onClick(position, data)
                }
            }

        }
    }
}