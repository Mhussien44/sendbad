package com.infovass.sindbad.View.Documents

import android.Manifest
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.infovass.sindbad.BuildConfig
import com.infovass.sindbad.ItemClickListener
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.DEBUG
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.PdfPreview.RemotePDFFragment
import com.infovass.sindbad.data.model.DocumentResultsItem
import com.kaopiz.kprogresshud.KProgressHUD
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.cutsom_dialog_layout.*
import kotlinx.android.synthetic.main.documents_fragment.*
import java.io.File


class DocumentsFragment : Fragment(), ItemClickListener {

    companion object {
        fun newInstance() = DocumentsFragment()
    }

    private var viewModel: DocumentsViewModel? = null
    private lateinit var adapter: DocumentsAdapter
    private var dialog: Dialog? = null
    private var completedRequestsCount = 0
    private var downloadId = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.documents_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.documents)

        if (viewModel == null)
            viewModel = ViewModelProviders.of(this).get(DocumentsViewModel::class.java)

        val progress = CustomProgressBar.showProgressBar(context!!)
        progress.show()

        initDocumentsRecycler()
        getUserInfo(progress)
        setUpUserSubjects(progress)
        setUpUserDocumentsCategories(progress)

        subjectSpinner.setOnSpinnerItemSelectedListener { parent, view, position, id ->
            getDocuments(progress)
        }

        documentsTypeSpinner.setOnSpinnerItemSelectedListener { parent, view, position, id ->
            getDocuments(progress)
        }
    }

    private fun setUpUserDocumentsCategories(progress: KProgressHUD) {
        viewModel?.documentsCategories()?.observe(this, Observer { names ->
            if (names != null) {
                completedRequestsCount++
                documentsTypeSpinner.attachDataSource(names)
                getDocumentsIfAllUserDataLoaded(progress)
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })

    }

    private fun getDocumentsIfAllUserDataLoaded(progress: KProgressHUD) {
        if (completedRequestsCount == 3) {
            progress.dismiss()
            getDocuments(progress)
            completedRequestsCount = 0
        }
    }

    private fun setUpUserSubjects(progress: KProgressHUD) {
        viewModel?.subjectsNames()?.observe(this, Observer { subjectsNames ->
            if (subjectsNames != null) {
                subjectSpinner.attachDataSource(subjectsNames)
                completedRequestsCount++
                getDocumentsIfAllUserDataLoaded(progress)
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })
    }

    //invoke when download or preview icons clicked
    override fun onClick(position: Int, document: DocumentResultsItem) {
        // if user press to download item
        if (document.actionType == "download") {
            checkPermissionToDownload(document)
        } else {
            fragmentManager?.inTransaction {
                replace(
                    R.id.mainContainer, RemotePDFFragment.newInstance(
                        document.file.toString(), document.name.toString()
                    )
                ).addToBackStack("RemotePDFFragment")
            }
        }
    }

    private fun checkPermissionToDownload(document: DocumentResultsItem) {
        val permissions = ArrayList<String>()
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        Dexter.withActivity(activity)
            .withPermissions(permissions)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                }

                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (!report?.isAnyPermissionPermanentlyDenied!!) {
                        downloadFiles(document.file.toString(), "${document.name}.pdf")
                    } else
                        Toast.makeText(context, " Permission denied", Toast.LENGTH_SHORT).show()

                }
            }).check()
    }

    private fun downloadFiles(url: String, fileName: String) {
        val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)

        downloadId = PRDownloader.download(url, dirPath.path, fileName)
            .build()
            .setOnStartOrResumeListener {
                showDialog(fileName)
            }
            .setOnPauseListener {
                dialog?.animatedCircleLoadingView?.stopFailure()
            }
            .setOnCancelListener {
                dialog?.animatedCircleLoadingView?.stopFailure()
            }
            .setOnProgressListener { progress ->
                val dProgress = (progress.currentBytes.toDouble() / progress.totalBytes.toDouble()) * 100.0
                dialog?.animatedCircleLoadingView?.setPercent(dProgress.toInt())

            }
            .start(object : OnDownloadListener {
                override fun onError(error: com.downloader.Error?) {
                    dialog?.dismiss()

                }

                override fun onDownloadComplete() {
                    dialog?.openDownload?.visibility = View.VISIBLE
                    dialog?.animatedCircleLoadingView?.stopOk()
                }

            })
    }

    private fun openFileAfterDownloaded(fileName: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            val file = File(
                (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)),
                fileName
            )
            val fileUri = FileProvider.getUriForFile(
                context!!,
                BuildConfig.APPLICATION_ID + ".provider", file
            )

            val intent = Intent(Intent.ACTION_VIEW)
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            intent.setDataAndType(fileUri, "application/pdf")
            startActivity(intent)
        } else {
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).path + File.separator +
                        fileName
            )
            val path = Uri.fromFile(file)
            val pdfOpenintent = Intent(Intent.ACTION_VIEW)
            pdfOpenintent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            pdfOpenintent.setDataAndType(path, "application/pdf")
            try {
                activity?.startActivity(pdfOpenintent)
            } catch (e: ActivityNotFoundException) {
               // e.printStackTrace()
            }

        }
    }

    override fun onDestroy() {
        PRDownloader.cancelAll()
        super.onDestroy()
    }

    private fun getUserInfo(progress: KProgressHUD) {
        viewModel?.getUser()?.observe(this, Observer { user ->
            completedRequestsCount++
            getDocumentsIfAllUserDataLoaded(progress)

            if (user != null) {
                if (user.success != null) {
                    docUserName.text = user.success.name
                    docUserGroup.text = user.success.group?.name
                    docUserTeam.text = user.success.group?.team?.name
                }
            } else makeToast(context!!, resources.getString(R.string.errors))
        })
    }

    private fun initDocumentsRecycler() {
        adapter = DocumentsAdapter(context!!, this)
        documentsList.layoutManager = LinearLayoutManager(context!!)
        documentsList.adapter = adapter
    }

    private fun getDocuments(progress: KProgressHUD) {
        if (isDocumentCatAndSubjectSelected()) {
            progress.show()
            viewModel?.getDocuments(getDocumentsAttributs())?.observe(this, Observer { documents ->
                progress.dismiss()
                if (documents != null) {
                    if (documents.checkErrors == false) {
                        noDocumentsAdded.visibility = View.GONE
                        documentsList.visibility = View.VISIBLE
                        adapter.setDocuments(documents.results)
                        adapter.notifyDataSetChanged()
                    }
                    if (documents.results.isNullOrEmpty()) {
                        documentsList.visibility = View.GONE
                        noDocumentsAdded.visibility = View.VISIBLE
                    }
                } else
                    makeToast(context!!, resources.getString(R.string.errors))
            })
        }

    }

    private fun isDocumentCatAndSubjectSelected(): Boolean {
        val documentCat = viewModel?.getDocumentCategoryId(documentsTypeSpinner.selectedIndex)
        val subjectId = viewModel?.getSubjectId(subjectSpinner.selectedIndex)
        return when {
            documentCat == null -> {
                makeToast(context!!, resources.getString(R.string.selectDocummentType))
                false
            }
            subjectId == null -> {
                makeToast(context!!, resources.getString(R.string.selectSubject))
                false
            }
            else -> true
        }
    }

    private fun getDocumentsAttributs(): HashMap<String, String> {
        val documentCat = viewModel?.getDocumentCategoryId(documentsTypeSpinner.selectedIndex)
        val subjectId = viewModel?.getSubjectId(subjectSpinner.selectedIndex)
        if (DEBUG) Log.i("documentCat", "${documentsTypeSpinner.selectedIndex} ..")
        val map = HashMap<String, String>()
        map["category_id"] = documentCat.toString()
        map["subject_id"] = subjectId.toString()
        return map
    }

    private fun showDialog(fileName: String) {
        try {
            dialog = Dialog(context!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.cutsom_dialog_layout)
            dialog?.animatedCircleLoadingView?.startDeterminate()
            dialog?.cancelDownload?.setOnClickListener {
                PRDownloader.cancel(downloadId)
                dialog?.dismiss()
            }
            dialog?.openDownload?.setOnClickListener {
                dialog?.dismiss()
                openFileAfterDownloaded(fileName)
            }
            dialog?.show()
        } catch (e: Exception) {

        }

    }

}
