package com.infovass.sindbad.View

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.infovass.sindbad.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_full_screen_image.*

class FullScreenImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)

        close.setOnClickListener {
            onBackPressed()
        }

        val img = intent?.getStringExtra("image")
        if (img != null)
            Picasso.get().load(img).placeholder(R.drawable.splashbg).error(R.drawable.splashbg)
                .into(image)
    }
}
