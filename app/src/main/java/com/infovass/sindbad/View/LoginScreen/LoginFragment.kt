package com.infovass.sindbad.View.LoginScreen

import android.app.Dialog
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.*
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.View.AccountActivation.AccountActivationFragment
import com.infovass.sindbad.View.Register.RegisterFragment
import com.infovass.sindbad.View.ResetPassword.ResetPasswordFragment
import com.infovass.sindbad.View.main.MainFragment
import com.infovass.sindbad.data.model.User
import com.kaopiz.kprogresshud.KProgressHUD
import com.onesignal.OneSignal
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.register_fragment.*
import kotlinx.android.synthetic.main.reset_pass_dialog.*

class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel
    private var progress: KProgressHUD? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        progress = CustomProgressBar.showProgressBar(context!!)

        initAppBar()

        loginBut.setOnClickListener {
            if (isValidUserInputs()) {
                progress?.show()
                viewModel.login(getLoginCredentials()).observe(this, Observer { user ->
                    progress?.dismiss()
                    if (user != null) {
                        if (user.accessToken != null && user.success != null) {
                            handleLoginSuccess(user)
                        } else {
                            when {
                                user.errors?.phone != null -> makeToast(context!!, user.errors.phone[0].toString())
                                else -> makeToast(context!!, user.message.toString())
                            }
                        }
                    } else
                        makeToast(context!!, resources.getString(R.string.errors))
                })
            }

        }
        loginRegister.setOnClickListener {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer, RegisterFragment.newInstance()).addToBackStack("RegisterFragment")
            }
        }
        resetPassword.setOnClickListener {
            showDialog()
        }
    }

    private fun initAppBar() {
        activity?.appBarTitle?.text = resources.getString(R.string.login)
        activity?.appMenu?.visibility = View.GONE
        activity?.barNotification?.visibility = View.GONE
    }

    private fun handleLoginSuccess(user: User) {
        if (user.success?.phoneCode == null)
        {
            makeToast(context!!, resources.getString(R.string.success))
            storeUserData(user)
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer, MainFragment.newInstance())
            }
        }
        else
            goToActivationAccount(user)
    }

    private fun goToActivationAccount(user: User) {
        fragmentManager?.inTransaction {
            replace(R.id.mainContainer ,
                AccountActivationFragment.newInstance(user.accessToken,user.success?.phone!!))
        }
    }

    private fun storeUserData(user: User?) {
        SharedPreferenceUtil(context!!).apply {
            putString(USERID, user?.success?.id.toString())
            putString(TOKEN, user?.accessToken.toString())
            putString(USERNAME, user?.success?.name.toString())
            putString(USERPHONE, user?.success?.phone.toString())
        }
    }

    private fun isValidUserInputs(): Boolean {
        val phone = loginPhone.text.toString()
        val pass = loginPass.text.toString()
        return if (!ValidationUtils.isValidPassword(pass)) {
            loginPass.error = resources.getString(R.string.correct_pass)
            false
        } else if (!ValidationUtils.isValidPhone(phone)) {
            loginPhone.error = resources.getString(R.string.correct_phone)
            false
        } else
            true
    }

    private fun getLoginCredentials(): HashMap<String, String> {
        val androidId = Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)
        val map = HashMap<String, String>()
        map["phone"] = loginPhone.text.toString()
        map["password"] = loginPass.text.toString()
        map["mac_address"] = androidId.toString()
        OneSignal.idsAvailable { userId, registrationId ->
            map["player_id"] = userId
        }
        return map
    }

    private fun showDialog() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.reset_pass_dialog)
        dialog.resetSendPhoneBut.setOnClickListener {
            sendResetPasswordRequest(dialog.resetPhone.text.toString(), dialog)
        }
        dialog.show()
    }

    private fun sendResetPasswordRequest(phone: String, dialog: Dialog) {
        if (phone.isNotEmpty()) {
            dialog.resetPassProgress.visibility = View.VISIBLE
            dialog.resetSendPhoneBut.visibility = View.GONE
            viewModel.resetPassRequest(phone).observe(this, Observer { result ->
                dialog.resetPassProgress.visibility = View.GONE
                dialog.resetSendPhoneBut.visibility = View.VISIBLE
                if (result != null) {
                    if (result.checkErrors == false) {
                        dialog.dismiss()
                        fragmentManager?.inTransaction {
                            replace(R.id.mainContainer, ResetPasswordFragment.newInstance(phone)).addToBackStack("loginFragment")
                        }
                    } else
                        makeToast(context!!, result.message.toString())

                } else
                    makeToast(context!!, resources.getString(R.string.errors))
            })
        } else
            makeToast(context!!, resources.getString(R.string.enterPhoneNumber))

    }
}
