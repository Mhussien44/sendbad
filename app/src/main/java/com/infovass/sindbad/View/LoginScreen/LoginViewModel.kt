package com.infovass.sindbad.View.LoginScreen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.repository.AuthRepository

class LoginViewModel (application: Application) : AndroidViewModel(application){

    val authRepository = AuthRepository("")

    fun login(map: HashMap<String,String>): MutableLiveData<User> {
        return authRepository.login(map)
    }

    fun resetPassRequest(phone: String): MutableLiveData<User> {
        return authRepository.resetPassRequest(phone)
    }


}
