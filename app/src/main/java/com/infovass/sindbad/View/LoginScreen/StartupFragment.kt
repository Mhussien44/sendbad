package com.infovass.sindbad.View.LoginScreen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.Register.RegisterFragment
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.startup_fragment.*

class StartupFragment : Fragment() {

    companion object {
        fun newInstance() = StartupFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.startup_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initAppBar()
        newUser.setOnClickListener {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer, RegisterFragment.newInstance()).addToBackStack("StartupFragment")
            }
        }

        currentUser.setOnClickListener {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer, LoginFragment.newInstance()).addToBackStack("StartupFragment")
            }
        }
    }

    private fun initAppBar() {
        activity?.appBarTitle?.text = resources.getString(R.string.app_name)
        activity?.appMenu?.visibility = View.GONE
        activity?.barNotification?.visibility = View.GONE
    }
}
