package com.infovass.sindbad.View.MessageDetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.USERID
import com.infovass.sindbad.data.model.CommentsItem
import kotlinx.android.synthetic.main.message_details_item.view.*

class MessageDetailsAdapter(private val context: Context) :
    RecyclerView.Adapter<MessageDetailsAdapter.ViewHolder>() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var messages: ArrayList<CommentsItem>? =  ArrayList()

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.message_details_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = messages?.get(position)
        holder.bind(data)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (messages.isNullOrEmpty())
            0
        else
            messages?.size!!
    }

    fun setMessagesList(newMessages: ArrayList<CommentsItem>) {
        messages = newMessages
    }

    fun addItemToMessages(message: CommentsItem) {
        messages?.add(message)
    }


    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(data: CommentsItem?) {
            if (data?.commentableType == "App\\Student") {
                initMessageItemIfReplyFromStudent()
            } else {
                initMessageItemIfReplyFromAdmin()
            }
            itemView.messageComment.text = data?.text
        }

        private fun initMessageItemIfReplyFromAdmin() {
            itemView.messageComment.background = context.resources.getDrawable(R.drawable.rounded_react_gray)
            itemView.messageComment.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
            itemView.messageCorner.background = context.resources.getDrawable(R.drawable.corner_gray)
            itemView.userIcon.setImageResource(R.drawable.logo_message)
            itemView.layoutDirection = View.LAYOUT_DIRECTION_LTR
        }

        private fun initMessageItemIfReplyFromStudent() {
            itemView.messageComment.background = context.resources.getDrawable(R.drawable.rounded_rect)
            itemView.messageCorner.background = context.resources.getDrawable(R.drawable.corner)
            itemView.messageComment.setTextColor(context.resources.getColor(R.color.white))
            itemView.userIcon.setImageResource(R.drawable.profile)
            itemView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        }
    }
}