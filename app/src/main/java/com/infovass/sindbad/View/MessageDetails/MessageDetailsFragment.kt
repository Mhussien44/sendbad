package com.infovass.sindbad.View.MessageDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.IOnBackPressed
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.main.MainFragment
import com.infovass.sindbad.data.model.CommentsItem
import com.infovass.sindbad.data.model.MessageResultsItem
import com.infovass.sindbad.data.model.SendMessageResponse
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.message_details_fragment.*

class MessageDetailsFragment : Fragment(), IOnBackPressed {

    companion object {
        fun newInstance(messages: MessageResultsItem?, fromNotification: Boolean) = MessageDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable("messages", messages)
                putBoolean("fromNotification", fromNotification)
            }
        }
    }

    private lateinit var viewModel: MessageDetailsViewModel
    private lateinit var adapter: MessageDetailsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.message_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.message)

        viewModel = ViewModelProviders.of(this).get(MessageDetailsViewModel::class.java)
        adapter = MessageDetailsAdapter(context!!)

        val messages = arguments?.getParcelable<MessageResultsItem>("messages") as MessageResultsItem

        messageComments.layoutManager = LinearLayoutManager(context)
        messageComments.adapter = adapter
        messageDetailsTitle.text = messages.title
        messageDetailsDesc.text = messages.text

        setupMessagesView(messages)
        messages.let { sendReply(messages) }
    }

    private fun setupMessagesView(messages: MessageResultsItem) {
        if (!messages.comments.isNullOrEmpty()) {
            noComments.visibility = View.GONE
            messageComments.visibility = View.VISIBLE
            adapter.setMessagesList(messages.comments as ArrayList<CommentsItem>)
        } else {
            messageComments.visibility = View.GONE
            noComments.visibility = View.VISIBLE
        }
    }

    private fun sendReply(message: MessageResultsItem) {
        sendReply.setOnClickListener {
            val text = addReplyEditText.text.toString()
            if (message.terminatedAt == null) {
                if (text.isNotEmpty()) {
                    sendProgressBar.visibility = View.VISIBLE
                    sendReplyForThisMessage(message)
                } else
                    addReplyEditText.error = resources.getString(R.string.message_requird)
            } else
                makeToast(context!!, resources.getString(R.string.cannot_reply))
        }

    }

    private fun sendReplyForThisMessage(message: MessageResultsItem) {
        val text = addReplyEditText.text.toString()
        viewModel.messageReply(message.id!!, text).observe(this, Observer { result ->
            sendProgressBar.visibility = View.GONE
            if (result != null) {
                if (result.checkErrors == false) {
                    updateUiAfterMessageSendSuccess(result)
                } else
                    makeToast(context!!, result.message.toString())
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })
    }

    private fun updateUiAfterMessageSendSuccess(messageResponse: SendMessageResponse) {
        noComments.visibility = View.GONE
        messageComments.visibility = View.VISIBLE
        addReplyEditText.setText("")
        messageResponse.results?.let { it1 -> adapter.addItemToMessages(it1) }
        adapter.notifyDataSetChanged()
        messageComments.scrollToPosition(adapter.itemCount - 1)
    }

    override fun onBackPressed(): Boolean {
        val fromNotification = arguments?.getBoolean("fromNotification")
        return if (fromNotification == true) {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer, MainFragment.newInstance())
            }
            false
        } else
            true
    }
}
