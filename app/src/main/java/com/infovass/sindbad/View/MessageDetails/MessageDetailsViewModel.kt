package com.infovass.sindbad.View.MessageDetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.SendMessageResponse
import com.infovass.sindbad.data.repository.MessagesRepository

class MessageDetailsViewModel (application: Application) : AndroidViewModel(application) {

    val token = SharedPreferenceUtil(getApplication()).getString(TOKEN,"")
    val repository = MessagesRepository(token.toString())

    fun messageReply(messageID: Int, text: String): MutableLiveData<SendMessageResponse> {
        return repository.messageReply(messageID , text)
    }
}
