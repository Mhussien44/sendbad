package com.infovass.sindbad.View.MessagesScreen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.MessageDetails.MessageDetailsFragment
import com.infovass.sindbad.data.model.MessageResultsItem
import kotlinx.android.synthetic.main.message_item.view.*

class MessagesAdapter(
    context: Context,
    private var messages: List<MessageResultsItem>,
    private var fragmentManager: FragmentManager?

) :
    RecyclerView.Adapter<MessagesAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.message_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = messages.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (messages.isNullOrEmpty())
            0
        else
            messages.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(data: MessageResultsItem?, position: Int) {
            itemView.messageTitle.text = data?.title
            itemView.messageDesc.text = data?.createdAt
            itemView.commentsCountNumber.text = data?.comments?.size.toString()

            itemView.setOnClickListener {
                fragmentManager?.inTransaction {
                    replace(R.id.mainContainer , MessageDetailsFragment.newInstance(data, false)).addToBackStack("MessageDetailsFragment")
                }
            }
        }
    }
}