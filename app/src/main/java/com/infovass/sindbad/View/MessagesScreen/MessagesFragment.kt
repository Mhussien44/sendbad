package com.infovass.sindbad.View.MessagesScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.ContactUs.ContactFragment
import com.infovass.sindbad.View.MessageDetails.MessageDetailsFragment
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.messages_fragment.*

class MessagesFragment : Fragment() {

    companion object {
        fun newInstance() = MessagesFragment()
    }

    private lateinit var viewModel: MessagesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.messages_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.message)

        val progress = CustomProgressBar.showProgressBar(context!!)
        progress.show()
        messagesList.layoutManager = LinearLayoutManager(context!!)

        viewModel = ViewModelProviders.of(this).get(MessagesViewModel::class.java)
        viewModel.getMyMessages().observe(this, Observer { messages ->
            progress.dismiss()
            if (messages != null) {
                if (messages.checkErrors == false) {
                    if (messages.results.isNullOrEmpty()) {
                       // noMessages.visibility = View.VISIBLE
                        // messagesList.visibility = View.GONE
                        fragmentManager?.inTransaction {
                            replace(R.id.mainContainer, ContactFragment.newInstance())
                        }
                    } else {
                        fragmentManager?.inTransaction {
                            replace(
                                R.id.mainContainer,
                                MessageDetailsFragment.newInstance(messages.results.last(), false)
                            )
                        }
                        // messagesList.adapter =
                        //   MessagesAdapter(context!!, messages.results as List<MessageResultsItem>, fragmentManager)
                    }
                }
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })
    }

}
