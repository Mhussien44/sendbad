package com.infovass.sindbad.View.MessagesScreen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.Messages
import com.infovass.sindbad.data.repository.MessagesRepository

class MessagesViewModel(application: Application) : AndroidViewModel(application) {

    val token = SharedPreferenceUtil(getApplication()).getString(TOKEN,"")
    val repository = MessagesRepository(token.toString())
    fun getMyMessages(): MutableLiveData<Messages> {
        return repository.getMessages()
    }
}
