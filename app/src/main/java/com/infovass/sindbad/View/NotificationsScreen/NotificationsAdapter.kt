package com.infovass.sindbad.View.NotificationsScreen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.R
import com.infovass.sindbad.data.model.NotificationItem
import kotlinx.android.synthetic.main.notification_item.view.*

class NotificationsAdapter (
    context: Context,
    private var messages: List<NotificationItem>
) :
    RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.notification_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = messages.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (messages.isNullOrEmpty())
            0
        else
            messages.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(data: NotificationItem?, position: Int) {
            itemView.notificationTitle.text = data?.title
            itemView.notificationDesc.text = data?.message
            itemView.notificationDate.text = data?.createdAt
        }
    }
}