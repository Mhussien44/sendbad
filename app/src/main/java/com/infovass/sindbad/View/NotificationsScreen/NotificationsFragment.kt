package com.infovass.sindbad.View.NotificationsScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.data.model.NotificationItem
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.notifications_fragment.*

class NotificationsFragment : Fragment() {

    companion object {
        fun newInstance() = NotificationsFragment()
    }

    private lateinit var viewModel: NotificationsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.notifications_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        activity?.appBarTitle?.text = resources.getString(R.string.notification)

        val progress = CustomProgressBar.showProgressBar(context!!)
        progress.show()

        notificationsList.layoutManager = LinearLayoutManager(context!!)

        viewModel.getNotification().observe(this, Observer { notifications ->
            progress.dismiss()
            if (notifications != null) {
                if (notifications.checkErrors == false) {
                    if (!notifications.results.isNullOrEmpty()) {
                        noNotifications.visibility = View.GONE
                        notificationsList.visibility = View.VISIBLE
                        notificationsList.adapter = NotificationsAdapter(context!! ,
                            notifications.results as List<NotificationItem>
                        )

                    }
                } else
                    makeToast(context!!, resources.getString(R.string.errors))

            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })

    }

}
