package com.infovass.sindbad.View.ProfileScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.ChangePassword.ChangePassFragment
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.profile_fragment.*

class ProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.profile)

        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        val progress = CustomProgressBar.showProgressBar(context!!)
        getUserInfo(progress)


        profileChangePass.setOnClickListener {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer, ChangePassFragment.newInstance()).addToBackStack("ChangePassFragment")
            }
        }
    }


    private fun getUserInfo(progress: KProgressHUD) {
        progress.show()
        viewModel.getUser().observe(this, Observer { user ->
            progress.dismiss()
            if (user != null) {
                if (user.success != null) {
                    profileUserName.text = user.success.name
                    profilePhone.text = user.success.phone
                    profileUserLevel.text = "${user.success.group?.name} - ${user.success.group?.team?.name}"
                    //profileSubscribedSubjects
                }
            } else makeToast(context!!, resources.getString(R.string.errors))
        })
    }

}
