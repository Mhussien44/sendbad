package com.infovass.sindbad.View.ProfileScreen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.RegistrationDataModel
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.repository.AppRepository
import com.infovass.sindbad.data.repository.AuthRepository

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var teams: MutableLiveData<RegistrationDataModel>
    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN,"")
    private val repository = AppRepository(token.toString())
    private val authRepository = AuthRepository("")

    fun getUser(): MutableLiveData<User> {
        return repository.getUser()
    }

    fun resetPassRequest(phone: String): MutableLiveData<User> {
        return authRepository.resetPassRequest(phone)
    }
}
