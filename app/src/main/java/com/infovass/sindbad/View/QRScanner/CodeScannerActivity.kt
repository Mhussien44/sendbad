package com.infovass.sindbad.View.QRScanner

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.budiyev.android.codescanner.*
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.View.VideoActivity
import com.infovass.sindbad.data.repository.AppRepository
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.alert_dialog.*


class CodeScannerActivity : AppCompatActivity() {

    private var codeScanner: CodeScanner? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_scanner)
        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)

        Dexter.withActivity(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                }

                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    setupQrScanner(scannerView)
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    finish()
                }
            }).check()
    }

    private fun setupQrScanner(scannerView: CodeScannerView) {
        codeScanner = CodeScanner(this, scannerView)

        // Parameters (default values)
        codeScanner?.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner?.formats = CodeScanner.ALL_FORMATS // list of actionType BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner?.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner?.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner?.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner?.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner?.decodeCallback = DecodeCallback {
            runOnUiThread {
                qrResult(it.text)
            }
        }
        codeScanner?.errorCallback = ErrorCallback {
            // or ErrorCallback.SUPPRESS
            runOnUiThread {
                Toast.makeText(
                    this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        scannerView.setOnClickListener {
            codeScanner?.startPreview()
        }
    }

    private fun qrResult(result: String) {
        val token = SharedPreferenceUtil(this).getString(TOKEN, "")
        val repository = AppRepository("$token")
        repository.validateCodeScanner(result).observe(this, Observer { qrResult ->
            if (qrResult != null) {
                if (qrResult.checkErrors == false) {
                    val videoLink = qrResult.results?.link
                    //   val videoLink = "http://mirrors.standaloneinstaller.com/video-sample/jellyfish-25-mbps-hd-hevc.mp4"
                    //    GiraffePlayer.play(activity!!, VideoInfo(videoLink)) \
                    //   MKPlayerActivity.configPlayer(this).play(videoLink)
                    if (!qrResult.results?.link.isNullOrEmpty()) {
                        val intent = Intent(this, VideoActivity::class.java)
                        intent.putExtra("videoLink", qrResult.results?.link)
                        startActivity(intent)
                    }
                } else
                    showDialog("${qrResult.message}")

            } else {
                Toast.makeText(this, resources.getString(R.string.errors), Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun showDialog(message: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.alert_dialog)
        dialog.alertDialogMesage.text = message
        dialog.alertDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    override fun onResume() {
        super.onResume()
        codeScanner?.startPreview()
    }

    override fun onPause() {
        codeScanner?.releaseResources()
        super.onPause()
    }
}