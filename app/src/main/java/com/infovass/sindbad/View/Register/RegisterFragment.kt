package com.infovass.sindbad.View.Register

import android.annotation.SuppressLint
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.ValidationUtils.*
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.AccountActivation.AccountActivationFragment
import com.kaopiz.kprogresshud.KProgressHUD
import com.onesignal.OneSignal
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.register_fragment.*

class RegisterFragment : Fragment() {

    companion object {
        fun newInstance() = RegisterFragment()
    }

    private lateinit var viewModel: RegisterViewModel
    private var progress : KProgressHUD? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.register)

        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        progress = CustomProgressBar.showProgressBar(context!!)

        getTeams()
        registerTeamSpinner.setOnSpinnerItemSelectedListener { parent, view, position, id ->
            // val teamId = viewModel.getTeamIdByName(registerTeamSpinner.selectedItem.toString())
            getGroupsForTeam(registerTeamSpinner.selectedIndex)
        }
    }

    private fun goToActivationAccount(accessToken: String?) {
        val phone = registerPhone.text.toString()
        fragmentManager?.inTransaction {
            replace(R.id.mainContainer , AccountActivationFragment.newInstance(accessToken,phone)).addToBackStack("AccountActivationFragment")
        }
    }

    private fun isValidUserInputs(): Boolean {
        val name = registerName.text.toString()
        val phone = registerPhone.text.toString()
        val pass = `registerPassُ`.text.toString()
        val confirmPass = `registerConfirmPassُ`.text.toString()
        val groupId = viewModel.getGrouopIdByName(registerSubjectSpinner.selectedItem.toString())

        return if (!isValidUserName(name)) {
            registerName.error = resources.getString(R.string.correct_name)
            false
        } else if (!isValidPhone(phone)) {
            registerPhone.error = resources.getString(R.string.correct_phone)
            false
        } else if (!isValidPassword(pass)) {
            `registerPassُ`.error = resources.getString(R.string.correct_pass)
            false

        } else if (pass != confirmPass) {
            `registerConfirmPassُ`.error = resources.getString(R.string.mismatch_pass)
            false

        } else if (groupId == 0 || groupId == null) {
            makeToast(context!!, resources.getString(R.string.selectGroup))
            false
        } else
            true
    }

    @SuppressLint("HardwareIds")
    private fun userRegistrationInfo(): HashMap<String, String> {
        val groupId = viewModel.getGrouopIdByName(registerSubjectSpinner.selectedItem.toString())
        val areasId = viewModel.getAreasIdByName(registerAreasSpinner.selectedItem.toString())
        val androidId = Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)

        val map = HashMap<String, String>()
        map["name"] = registerName.text.toString()
        map["phone"] = registerPhone.text.toString()
        map["group_id"] = groupId.toString()
        map["area_id"] = areasId.toString()
        map["password"] = `registerPassُ`.text.toString()
        map["password_confirmation"] = `registerConfirmPassُ`.text.toString()
        map["mac_address"] = androidId.toString()
        OneSignal.idsAvailable { userId, registrationId ->
            map["player_id"] = userId
        }
        return map
    }


    private fun getTeams() {
        progress?.show()
        viewModel.teamsNames().observe(this, Observer { teams ->
            progress?.dismiss()
            if (teams != null) {
                registerTeamSpinner.attachDataSource(teams)
                getGroupsForTeam(registerTeamSpinner.selectedIndex)
                getAreas()

            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })
    }

    private fun getAreas() {
        val areas = viewModel.getAreasNames()
        if (areas != null) {
            registerAreasSpinner.attachDataSource(areas)
        } else
            makeToast(context!!, resources.getString(R.string.errors))
    }

    private fun getGroupsForTeam(teamPosition: Int?) {
        viewModel.teamPosition = teamPosition!!
        val groups = viewModel.groupsNames()
        if (groups != null) {
            registerSubjectSpinner.attachDataSource(groups)
            onRegisterButClicked()
        } else
            makeToast(context!!, resources.getString(R.string.errors))
    }
//649914
    private fun onRegisterButClicked(){
        registerBut.setOnClickListener {
            if (isValidUserInputs()) {
                progress?.show()
                viewModel.register(userRegistrationInfo()).observe(this, Observer { user ->
                    progress?.dismiss()//464385
                    if (user != null) {
                        if (user.success != null && user.accessToken != null) {
                            goToActivationAccount(user.accessToken)
                        } else {
                            when {
                                user.errors?.phone != null -> makeToast(context!!, user.errors.phone[0].toString())
                                user.errors?.email != null -> makeToast(context!!, user.errors.email[0].toString())
                                else -> makeToast(context!!, user.message.toString())
                            }
                        }
                    } else
                        makeToast(context!!, resources.getString(R.string.errors))
                })
            }

        }
    }


}
