package com.infovass.sindbad.View.Register

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.GroupsItem
import com.infovass.sindbad.data.model.RegistrationDataModel
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.repository.AppRepository
import com.infovass.sindbad.data.repository.AuthRepository

class RegisterViewModel(application: Application) : AndroidViewModel(application) {

    private var teamsMap: HashMap<Int, String>? = null
    private var groupsMap: HashMap<Int, String>? = null
    private var areasMap: HashMap<Int, String>? = null
    private var registrationData: MutableLiveData<RegistrationDataModel>? = null
    val token = SharedPreferenceUtil(getApplication()).getString(TOKEN,"")

    var teamPosition = 0
    val repository = AppRepository(token.toString())

    val authRepository = AuthRepository(token.toString())

    init {
        teamsMap = HashMap()
        groupsMap = HashMap()
        areasMap = HashMap()
    }

    private fun getRegistrationData(): MutableLiveData<RegistrationDataModel> {
        registrationData = repository.getTeams()
        return registrationData!!
    }

    private fun getTeamGroups(): List<GroupsItem?>? {
        return registrationData?.value?.results?.teams?.get(teamPosition)?.groups
    }

    fun teamsNames() = Transformations.map(getRegistrationData()) { registrationData ->
        val teams = registrationData?.results?.teams
        if (teams != null) {
            val names = ArrayList<String>(teams.size)
            for (i in teams.indices) {
                names.add(teams[i]?.name.toString())
                teamsMap?.set(teams[i]?.id!!, teams[i]?.name.toString())
            }
            names
        } else
            null
    }

    fun groupsNames(): ArrayList<String>? {
        val groups = getTeamGroups()
        return if (groups != null) {
            val names = ArrayList<String>()
            for (i in groups.indices) {
                names.add(groups[i]?.name.toString())
                groupsMap?.set(groups[i]?.id!!, groups[i]?.name.toString())
            }
            names
        } else
            null
    }


    fun getGrouopIdByName(teamName: String): Int? {
        return groupsMap?.let { getKey(it, teamName) }
    }

    fun getAreasIdByName(areaName: String): Int? {
        return areasMap?.let { getKey(it, areaName) }
    }

    fun register(map: HashMap<String, String>): MutableLiveData<User> {
        return authRepository.register(map)
    }

    private fun <K, V> getKey(map: HashMap<K, V>, value: V): K? {
        for ((key, value1) in map) {
            if (value1 == value) {
                return key
            }
        }
        return null
    }

    fun getAreasNames() : ArrayList<String>? {
        val areas = registrationData?.value?.results?.areas
       return if (areas != null) {
            val names = ArrayList<String>(areas.size)
            for (i in areas.indices) {
                names.add(areas[i]?.name.toString())
                areasMap?.set(areas[i]?.id!!, areas[i]?.name.toString())
            }
            names
        } else
            null
    }
}