package com.infovass.sindbad.View.ResetPassword

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.ValidationUtils.isValidPassword
import kotlinx.android.synthetic.main.reset_password_fragment.*

class ResetPasswordFragment : Fragment() {

    companion object {
        fun newInstance(phone: String) = ResetPasswordFragment().apply {
            arguments = Bundle().apply {
                putString("phone", phone)
            }
        }
    }

    private lateinit var viewModel: ResetPasswordViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.reset_password_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ResetPasswordViewModel::class.java)

        val progress = CustomProgressBar.showProgressBar(context!!)
        moveCursorToNextEditText()

        resetPassBut.setOnClickListener {
            if (isValidInputs()) {
                progress.show()
                viewModel.resetPassword(getUserInputs()).observe(this, Observer { result ->
                    progress.dismiss()
                    if (result != null) {
                        if (result.checkErrors == false) {
                            makeToast(context!!, resources.getString(R.string.password_changed))
                            activity?.onBackPressed()
                        } else
                            makeToast(context!!, result.message.toString())
                    } else
                        makeToast(context!!, resources.getString(R.string.errors))
                })
            }

        }
    }

    private fun getUserInputs(): HashMap<String, String> {
        val map = HashMap<String, String>()
        map["password"] = changePass.text.toString()
        map["password_confirmation"] = changePassConfirm.text.toString()
        map["phone"] = arguments?.getString("phone").toString()
        map["code"] = getCode().toString()
        return map
    }

    private fun isValidInputs(): Boolean {
        return when {
            !isValidPassword(changePass.text.toString()) -> {
                changePass.error = resources.getString(R.string.correct_pass)
                false
            }
            !isValidPassword(changePassConfirm.text.toString()) -> {
                changePassConfirm.error = resources.getString(R.string.correct_pass)
                false
            }
            changePassConfirm.text.toString() != changePass.text.toString() -> {
                makeToast(context!!, resources.getString(R.string.mismatch_pass))
                false
            }
            !isValidCode() -> false
            else -> true
        }
    }

    private fun getCode(): String {
        val code = "${code1.text}${code2.text}${code3.text}${code4.text}${code5.text}${code6.text}"
        return code
    }

    private fun isValidCode(): Boolean {
        return if (getCode() != "") {
            if (getCode().length == 6)
                true
            else {
                makeToast(context!!, resources.getString(R.string.enterVaidCode))
                false
            }
        } else {
            makeToast(context!!, resources.getString(R.string.enterVaidCode))
            false
        }

    }

    private fun moveCursorToNextEditText() {
        code1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code2.requestFocus()
                }
            }

        })

        code2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code3.requestFocus()
                }
            }

        })

        code3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code4.requestFocus()
                }
            }

        })

        code4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code5.requestFocus()
                }
            }

        })

        code5.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text?.length == 1) {
                    code6.requestFocus()
                }
            }

        })

    }
}
