package com.infovass.sindbad.View.ResetPassword

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.repository.AuthRepository

class ResetPasswordViewModel(application: Application) : AndroidViewModel(application){

    val authRepository = AuthRepository("")

    fun resetPassword(map: HashMap<String,String>): MutableLiveData<User> {
        return authRepository.resetPassword(map)
    }
}
