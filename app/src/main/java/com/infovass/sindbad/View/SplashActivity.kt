package com.infovass.sindbad.View

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.infovass.sindbad.R
import com.infovass.sindbad.View.main.MainActivity
import kotlinx.android.synthetic.main.activity_splach.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splach)

        try {
            Glide.with(this)
                .load(resources.getDrawable(R.drawable.splashbg))
                .into(splashBg)
        } catch (e: Exception) {

        }

        /** Duration of wait  */
        val length = 2000
        /* New Handler to start the Main Activity
         * and close this Splash-Screen after 2 seconds.*/
        Handler().postDelayed({

            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, length.toLong())
    }
}
