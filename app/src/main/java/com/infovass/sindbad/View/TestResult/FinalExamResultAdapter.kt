package com.infovass.sindbad.View.TestResult

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.R
import com.infovass.sindbad.data.model.TestinresultsModel
import kotlinx.android.synthetic.main.final_result_item.view.*
import java.util.ArrayList

class FinalExamResultAdapter(
        private val context: Context,
        private var items: ArrayList<TestinresultsModel>,
        private val itemClickListener : ItemClickListener) :
        RecyclerView.Adapter<FinalExamResultAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.final_result_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (items.isNullOrEmpty())
            0
        else
            items.size
    }

    inner class ViewHolder constructor(itemView: View) :
            RecyclerView.ViewHolder(itemView) {
        fun bind(item: TestinresultsModel, position: Int) {
            itemView.question.text = item.title
            if (item.answer?.selectedAnswer == item.answer?.rightAnswer)
                itemView.imgResult.setImageResource(R.drawable.ic_result_success)
            else{
                itemView.imgResult.setImageResource(R.drawable.ic_result_error)
                val correctAnswerText =   when (item.answer?.rightAnswer) {
                    1 -> item.answer.answer1
                    2 -> item.answer.answer2
                    3 -> item.answer.answer3
                    4 -> item.answer.answer4
                    else -> item.answer?.answer1
                }
                itemView.setOnClickListener {
                    itemClickListener.onClick(correctAnswerText ?: "" , item.answer?.picture ?: "")
                }
            }
        }
    }
}

interface ItemClickListener{
    fun onClick(text : String, img : String)
}