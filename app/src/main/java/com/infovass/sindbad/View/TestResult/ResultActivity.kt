package com.infovass.sindbad.View.TestResult

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.R
import com.infovass.sindbad.data.model.TestinresultsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity(), ItemClickListener {

    private var testData =
        lazy { intent.getParcelableArrayListExtra<TestinresultsModel>("testData") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        var correctAnswersCount = 0
        val total = testData.value.size
        testData.value.forEach {
            if (it.answer?.rightAnswer == it.answer?.selectedAnswer)
                correctAnswersCount++

        }

        val percentage = (correctAnswersCount.toDouble() / total) * 100

        resultValue.text = "${percentage.toInt()} %"
        resultList.layoutManager = LinearLayoutManager(this)
        resultList.adapter = FinalExamResultAdapter(this, testData.value, this)

        close.setOnClickListener {
            wrongAnswerLayout.visibility = GONE
            resultList.visibility = VISIBLE
        }

        appMenu.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onClick(text: String, img: String) {
        correctAnswerText.text = text
        if (img != "")
            Picasso.get().load(img).placeholder(R.drawable.splashlogo).error(R.drawable.splashlogo).into(
                answerImg
            )
        wrongAnswerLayout.visibility = VISIBLE
        resultList.visibility = GONE
    }

}
