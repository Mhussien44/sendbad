package com.infovass.sindbad.View.Testing

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.R
import com.infovass.sindbad.data.model.SubjectsResults

class QuestionChoicesAdapter (
    private val context: Context,
    private var items: List<SubjectsResults?>?) :
    RecyclerView.Adapter<QuestionChoicesAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.question_choice_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items?.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (items.isNullOrEmpty())
            0
        else
            items!!.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(item: SubjectsResults?, position: Int) {

        }
    }
}