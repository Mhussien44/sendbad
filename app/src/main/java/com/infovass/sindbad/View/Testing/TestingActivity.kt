package com.infovass.sindbad.View.Testing

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.View.FullScreenImageActivity
import com.infovass.sindbad.View.TestResult.ResultActivity
import com.infovass.sindbad.data.model.AnswerModel
import com.infovass.sindbad.data.model.ExamResultsModel
import com.infovass.sindbad.data.model.TestinresultsModel
import com.kaopiz.kprogresshud.KProgressHUD
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_testing.*
import kotlinx.android.synthetic.main.alert_dialog.*

const val SELECTED_SUBJECT_ID = "selectedSubjectId"
const val SELECTED_CHAPTER = "selectedChapter"
const val QUESTIONS_COUNT = "QUESTIONSCOUNT"
const val TESTING_TYPE = "TESTINGTYPE"
const val UNIFIED_TESTING_DATA = "UNIFIED_TESTING_DATA"

class TestingActivity : AppCompatActivity() {

    private lateinit var viewModel: TestingViewModel
    private var progress: KProgressHUD? = null

    private var currentQuestionPosition = 0
    private val testingType = lazy { intent.getIntExtra(TESTING_TYPE, 1) }
    private val selectedSubjectId = lazy { intent.getIntExtra(SELECTED_SUBJECT_ID, 0) }
    private val selectedChapter = lazy { intent.getIntExtra(SELECTED_CHAPTER, 0) }
    private val questionsCount = lazy { intent.getIntExtra(QUESTIONS_COUNT, -1) }
    private var isWrongAnswerViewDisplayed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_testing)
        progress = CustomProgressBar.showProgressBar(this)

        viewModel = ViewModelProviders.of(this).get(TestingViewModel::class.java)

        if (testingType.value != 3)
            getTest()
        else {
            val testList =
                intent.getParcelableArrayListExtra<ExamResultsModel>(UNIFIED_TESTING_DATA)
            viewModel.testList.addAll(testList[0].publicExamQuestion!!.map {
                TestinresultsModel(
                    title = it?.examQuestion?.title.toString(),
                    picture = it?.examQuestion?.picture,
                    answer = AnswerModel(
                        id = it?.examAnswer?.id, answer1 = it?.examAnswer?.answer1,
                        answer2 = it?.examAnswer?.answer2,
                        answer3 = it?.examAnswer?.answer3,
                        answer4 = it?.examAnswer?.answer4,
                        rightAnswer = it?.examAnswer?.rightAnswer,
                        selectedAnswer = it?.examAnswer?.selectedAnswer,
                        picture = it?.examAnswer?.picture
                    )
                )
            })
            currentQuestionPosition = 0
            setupQuestionView()
        }

        appMenu.setOnClickListener {
            onBackPressed()
        }
        nextQuestionBut.setOnClickListener {
            choiceIcon.setImageResource(R.drawable.icon_check_hov)
            choiceBIcon.setImageResource(R.drawable.b_hov)
            choiceCIcon.setImageResource(R.drawable.c_hov)
            choiceDIcon.setImageResource(R.drawable.d_hov)
            if (testingType.value == 1 && !isRightAnswer()) {
                if (viewModel.testList.size - 1 < currentQuestionPosition) {
                    onBackPressed()
                } else
                    showWrongAnswerView()
            } else {
                currentQuestionPosition++
                if (viewModel.testList.size - 1 < currentQuestionPosition) {
                    val intent = Intent(this, ResultActivity::class.java)
                    intent.putExtra("testData", viewModel.testList)
                    startActivity(intent)
                    finish()
                } else
                    setupQuestionView()
            }
        }

        questionImg.setOnClickListener {
            val intent = Intent(this, FullScreenImageActivity::class.java)
            intent.putExtra("image", viewModel.testList[currentQuestionPosition]?.picture)
            startActivity(intent)
        }
    }

    private fun getTest() {
        nextQuestionBut.visibility = GONE
        progress?.show()
        val map = HashMap<String, String>()
        map["subject_id"] = selectedSubjectId.value.toString()
        map["chapter"] = selectedChapter.value.toString()
        if (questionsCount.value != -1) map["test_count"] = questionsCount.value.toString()
        map["type"] = testingType.value.toString()
        viewModel.getTest(map).observe(this, Observer {
            progress?.dismiss()
            nextQuestionBut.visibility = VISIBLE
            if (!it.isNullOrEmpty()) {
                viewModel.testList.addAll(it)
                currentQuestionPosition = 0
                setupQuestionView()
            } else {
                makeToast(this, getString(R.string.no_questions))
                onBackPressed()
            }
        })
    }

    private fun showWrongAnswerView() {
        isWrongAnswerViewDisplayed = true
        wrongAnswerLayout.visibility = VISIBLE
        questionLayout.visibility = GONE

        close.setOnClickListener {
            isWrongAnswerViewDisplayed = false
            currentQuestionPosition++
            if (viewModel.testList.size - 1 < currentQuestionPosition) {
                onBackPressed()
            } else {
                wrongAnswerLayout.visibility = GONE
                questionLayout.visibility = VISIBLE

                setupQuestionView()
            }
        }

        correctAnswerText.text =
            when (viewModel.testList[currentQuestionPosition]?.answer?.rightAnswer) {
                1 -> viewModel.testList[currentQuestionPosition]?.answer?.answer1
                2 -> viewModel.testList[currentQuestionPosition]?.answer?.answer2
                3 -> viewModel.testList[currentQuestionPosition]?.answer?.answer3
                4 -> viewModel.testList[currentQuestionPosition]?.answer?.answer4
                else -> viewModel.testList[currentQuestionPosition]?.answer?.answer1
            }

        answerImg.setOnClickListener {
            val intent = Intent(this, FullScreenImageActivity::class.java)
            intent.putExtra("image", viewModel.testList[currentQuestionPosition]?.answer?.picture)
            startActivity(intent)
        }

        viewModel.testList[currentQuestionPosition]?.answer?.picture.let {
            Picasso.get().load(it).placeholder(R.drawable.splashbg).error(R.drawable.splashbg)
                .into(answerImg)
        }
    }

    private fun isRightAnswer(): Boolean {
        return viewModel.testList[currentQuestionPosition]?.answer?.selectedAnswer ==
                viewModel.testList[currentQuestionPosition]?.answer?.rightAnswer
    }

    private fun setupQuestionView() {
        if (viewModel.testList.size - 1 < currentQuestionPosition)
            return
        if (viewModel.testList[currentQuestionPosition]?.picture != null) {
            questionImg.visibility = VISIBLE
            questionText.visibility = GONE
            Picasso.get().load(viewModel.testList[currentQuestionPosition]?.picture).into(
                questionImg
            )
        } else {
            questionImg.visibility = GONE
            questionText.visibility = VISIBLE
            questionText.text = viewModel.testList[currentQuestionPosition]?.title
        }

        choiceText.text = viewModel.testList[currentQuestionPosition]?.answer?.answer1
        choiceBText.text = viewModel.testList[currentQuestionPosition]?.answer?.answer2
        choiceCText.text = viewModel.testList[currentQuestionPosition]?.answer?.answer3
        choiceDText.text = viewModel.testList[currentQuestionPosition]?.answer?.answer4

        appBarTitle.text =
            "${resources.getString(R.string.question_number)} ${currentQuestionPosition + 1}"
        nextQuestionBut.visibility = GONE

        answerA.setOnClickListener {
            nextQuestionBut.visibility = VISIBLE
            choiceIcon.setImageResource(R.drawable.a)
            choiceBIcon.setImageResource(R.drawable.b_hov)
            choiceCIcon.setImageResource(R.drawable.c_hov)
            choiceDIcon.setImageResource(R.drawable.d_hov)
            viewModel.testList[currentQuestionPosition]?.answer?.selectedAnswer = 1
        }

        answerB.setOnClickListener {
            nextQuestionBut.visibility = VISIBLE
            viewModel.testList[currentQuestionPosition]?.answer?.selectedAnswer = 2

            choiceIcon.setImageResource(R.drawable.icon_check_hov)
            choiceBIcon.setImageResource(R.drawable.b)
            choiceCIcon.setImageResource(R.drawable.c_hov)
            choiceDIcon.setImageResource(R.drawable.d_hov)
        }

        answerC.setOnClickListener {
            nextQuestionBut.visibility = VISIBLE
            viewModel.testList[currentQuestionPosition]?.answer?.selectedAnswer = 3

            choiceIcon.setImageResource(R.drawable.icon_check_hov)
            choiceBIcon.setImageResource(R.drawable.b_hov)
            choiceCIcon.setImageResource(R.drawable.c)
            choiceDIcon.setImageResource(R.drawable.d_hov)
        }

        answerD.setOnClickListener {
            nextQuestionBut.visibility = VISIBLE
            viewModel.testList[currentQuestionPosition]?.answer?.selectedAnswer = 4

            choiceIcon.setImageResource(R.drawable.icon_check_hov)
            choiceBIcon.setImageResource(R.drawable.b_hov)
            choiceCIcon.setImageResource(R.drawable.c_hov)
            choiceDIcon.setImageResource(R.drawable.d)
        }
    }

    override fun onBackPressed() {
        if (isWrongAnswerViewDisplayed) {
            isWrongAnswerViewDisplayed = false
            wrongAnswerLayout.visibility = GONE
            questionLayout.visibility = VISIBLE
        } else if (viewModel.testList.isNullOrEmpty()) super.onBackPressed() else showDialog()

    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.alert_dialog)
        dialog.alertDialogMesage.visibility = VISIBLE
        dialog.alertDialogCount.visibility = GONE
        dialog.alertDialogEditText.visibility = GONE

        dialog.alertDialogTitle.text = resources.getString(R.string.alert)
        dialog.alertDialogMesage.text = resources.getString(R.string.close_test_hint)
        dialog.alertDialogOk.text = resources.getString(R.string.confirm)

        dialog.alertDialogOk.setOnClickListener {
            dialog.dismiss()
            super.onBackPressed()
        }
        dialog.alertDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

}
