package com.infovass.sindbad.View.Testing

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.TestinresultsModel
import com.infovass.sindbad.data.repository.AppRepository
import java.util.HashMap

class TestingViewModel(application : Application) : AndroidViewModel(application) {
    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")

    var testList = ArrayList<TestinresultsModel?>()
    val repository = AppRepository(token.toString())

    fun getTest(map: HashMap<String,String>): MutableLiveData<List<TestinresultsModel?>> {
        return repository.makeTesting(map)
    }
}