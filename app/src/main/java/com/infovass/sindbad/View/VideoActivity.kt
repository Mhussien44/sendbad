package com.infovass.sindbad.View

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.infovass.sindbad.R
import com.khizar1556.mkvideoplayer.MKPlayer

class VideoActivity : AppCompatActivity() {

    private lateinit var player: MKPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        val videoLink = intent.getStringExtra("videoLink")

        player = MKPlayer(this)
        player.isPlayerSupport
        player.play(videoLink)
        player.setFullScreenOnly(true)
        player.setPlayerCallbacks(object : MKPlayer.playerCallbacks {
            override fun onNextClick() {
                Log.e("onNextClick", "onNextClick")
            }

            override fun onPreviousClick() {
                Log.e("onPreviousClick", "onPreviousClick")

            }
        })

    }

    override fun onStop() {
        super.onStop()
        if (player.isPlaying) {
            player.stop()
            player.onDestroy()
        }
    }
}
