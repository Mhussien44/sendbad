package com.infovass.sindbad.View.educationVideo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
 import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SnackAndToastUtil
import com.infovass.sindbad.data.model.VideoBySubjectResult
import kotlinx.android.synthetic.main.activity_video.view.*
import kotlinx.android.synthetic.main.video_item.view.*

class EducationVideosAdapter constructor(
    private val context: Context,
    private val playItemClickListener: PlayItemClickListener
) :
    RecyclerView.Adapter<EducationVideosAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    private var videos: List<VideoBySubjectResult>? = null
    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.video_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = videos?.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (videos == null)
            0
        else
            videos!!.size
    }

    fun setVideos(videos: List<VideoBySubjectResult?>?){
        this.videos = videos as List<VideoBySubjectResult>?
    }
    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(data: VideoBySubjectResult?, position: Int) {
            itemView.videoName.text = data?.name
            itemView.viewsNo.text = data?.watch_count.toString()
            if(data!!.activated){
                if(data!!.watch_count>0){
                     itemView.playVideo.background= context.resources.getDrawable(R.drawable.play_button)
                }else{
                     itemView.playVideo.background= context.resources.getDrawable(R.drawable.stop_button)
                }

            }else{
                 itemView.playVideo.background= context.resources.getDrawable(R.drawable.stop_button)
            }
            itemView.playVideo.setOnClickListener {
                if (data != null) {


//
                    if(data.activated){
                        if(data.watch_count>0){
                            playItemClickListener.onPlayClick(position,data)
                        }else{
                            SnackAndToastUtil.makeToast(context, "برجاء مراجعة الإدارة")
                        }

                    }else{
                        SnackAndToastUtil.makeToast(context, "برجاء مراجعة الإدارة")
                    }

                }
            }

        }
    }


    interface PlayItemClickListener {
        fun onPlayClick(position: Int, video: VideoBySubjectResult)
    }
}