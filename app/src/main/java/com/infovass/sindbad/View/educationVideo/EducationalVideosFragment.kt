package com.infovass.sindbad.View.educationVideo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.MessageDetails.MessageDetailsFragment
import com.infovass.sindbad.View.videoDetails.VideosDetailsFragment
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.model.VideoBySubjectResult
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.documents_fragment.*
import kotlinx.android.synthetic.main.documents_fragment.docUserGroup
import kotlinx.android.synthetic.main.documents_fragment.docUserName
import kotlinx.android.synthetic.main.documents_fragment.docUserTeam
import kotlinx.android.synthetic.main.documents_fragment.noDocumentsAdded
import kotlinx.android.synthetic.main.documents_fragment.subjectSpinner
import kotlinx.android.synthetic.main.edu_video_fragment.*


class EducationalVideosFragment : Fragment() ,EducationVideosAdapter.PlayItemClickListener{

    companion object {
        fun newInstance() = EducationalVideosFragment()
    }

    var user: User?=null

    private var viewModel: EducationalVideosViewModel? = null
    private lateinit var adapter: EducationVideosAdapter
     private var completedRequestsCount = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.edu_video_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.appBarTitle?.text = resources.getString(R.string.documents2)

        if (viewModel == null)
            viewModel = ViewModelProviders.of(this).get(EducationalVideosViewModel::class.java)

        val progress = CustomProgressBar.showProgressBar(context!!)
        progress.show()

        initDocumentsRecycler()
        getUserInfo(progress)
        setUpUserSubjects(progress)
//        setUpUserDocumentsCategories(progress)

        subjectSpinner.setOnSpinnerItemSelectedListener { parent, view, position, id ->
            getVideos(progress)
        }


    }

    private fun setUpUserDocumentsCategories(progress: KProgressHUD) {
        viewModel?.documentsCategories()?.observe(this, Observer { names ->
            if (names != null) {
                completedRequestsCount++
//                documentsTypeSpinner.attachDataSource(names)
                getDocumentsIfAllUserDataLoaded(progress)
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })

    }

    private fun getDocumentsIfAllUserDataLoaded(progress: KProgressHUD) {
        if (completedRequestsCount == 1) {
            progress.dismiss()
            getVideos(progress)
            completedRequestsCount = 0
        }
    }

    private fun setUpUserSubjects(progress: KProgressHUD) {
        viewModel?.subjectsNames()?.observe(this, Observer { subjectsNames ->
            if (subjectsNames != null) {
                subjectSpinner.attachDataSource(subjectsNames)
                completedRequestsCount++
                getDocumentsIfAllUserDataLoaded(progress)
            } else
                makeToast(context!!, resources.getString(R.string.errors))
        })
    }








    private fun getUserInfo(progress: KProgressHUD) {
        viewModel?.getUser()?.observe(this, Observer { user ->
            completedRequestsCount++
            getDocumentsIfAllUserDataLoaded(progress)

            if (user != null) {
                this.user=user
                if (user.success != null) {
                    docUserName.text = user.success.name
                    docUserGroup.text = user.success.group?.name
                    docUserTeam.text = user.success.group?.team?.name
                }
            } else makeToast(context!!, resources.getString(R.string.errors))
        })
    }

    private fun initDocumentsRecycler() {
        adapter = EducationVideosAdapter(context!!, this)
        videosList.layoutManager = LinearLayoutManager(context!!)
        videosList.adapter = adapter
    }

    private fun getVideos(progress: KProgressHUD) {
        if (isDocumentCatAndSubjectSelected()) {
            progress.show()
            viewModel?.getEducationalVideos(getAttributes())?.observe(this, Observer { videos ->
                progress.dismiss()
                if (videos != null) {
                    if (!videos.check_errors) {
                        noDocumentsAdded.visibility = View.GONE
                        videosList.visibility = View.VISIBLE
                        adapter.setVideos(videos.results)
                        adapter.notifyDataSetChanged()
                    }
                    if (videos.results.isNullOrEmpty()) {
                        videosList.visibility = View.GONE
                        noDocumentsAdded.visibility = View.VISIBLE
                    }
                } else
                    makeToast(context!!, resources.getString(R.string.errors))
            })
        }

    }

    private fun isDocumentCatAndSubjectSelected(): Boolean {
//        val documentCat = viewModel?.getDocumentCategoryId(documentsTypeSpinner.selectedIndex)
        val subjectId = viewModel?.getSubjectId(subjectSpinner.selectedIndex)
        return if( subjectId == null){
            makeToast(context!!, resources.getString(R.string.selectSubject))
            false
        }else{
            true
        }
//        return when {
////            documentCat == null -> {
////                makeToast(context!!, resources.getString(R.string.selectDocummentType))
////                false
////            }
//            subjectId == null -> {
//                makeToast(context!!, resources.getString(R.string.selectSubject))
//                false
//            }
//            else -> true
//        }
    }

    private fun getAttributes(): HashMap<String, String> {
         val subjectId = viewModel?.getSubjectId(subjectSpinner.selectedIndex)
         val map = HashMap<String, String>()
         map["subjectId"] =  subjectId.toString()
        return map
    }

    override fun onPlayClick(position: Int, video: VideoBySubjectResult) {
        fragmentManager?.inTransaction {
            replace(
                R.id.mainContainer,
                VideosDetailsFragment.newInstance(video,user!!)
            )
        }
    }


}
