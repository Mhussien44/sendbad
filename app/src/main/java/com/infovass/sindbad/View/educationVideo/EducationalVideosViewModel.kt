package com.infovass.sindbad.View.educationVideo

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.*
import com.infovass.sindbad.data.repository.AppRepository

class EducationalVideosViewModel(application: Application) : AndroidViewModel(application) {

    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")
    private val repository = AppRepository(token.toString())
    private var documentsCategories: MutableLiveData<DocumentCategories>? = null
    private var user: MutableLiveData<User>? = null
    private var documents : MutableLiveData<Documents>? = null
    private var subjects: MutableLiveData<Subjects>? = null
    private var videos: MutableLiveData<VideoBySubjectResponse>? = null

    private fun getSubjects(): MutableLiveData<Subjects>? {
        return if (subjects == null) {
            subjects = repository.getSubjects()
            subjects
        } else
            subjects

    }


      fun getEducationalVideos(map: HashMap<String, String>): MutableLiveData<VideoBySubjectResponse>? {

            videos = repository.getEducationalVideos(map)
          return  videos


    }
    fun subjectsNames() = getSubjects()?.let {
        Transformations.map(it) { teams ->
            if (teams != null) {
                val names = ArrayList<String>(teams.results?.size!!)
                for (i in teams.results.indices) {
                    names.add(teams.results[i]?.name.toString())
                }
                names
            } else
                null
        }
    }

    private fun getDocumentCategories(): MutableLiveData<DocumentCategories>? {
        return if (documentsCategories == null) {
            documentsCategories = repository.getDocumentCategories()
            documentsCategories
        } else
            documentsCategories
    }



    fun documentsCategories() = getDocumentCategories()?.let {
        Transformations.map(it) { teams ->
        if (teams != null) {
            val names = ArrayList<String>(teams.results?.size!!)
            for (i in teams.results.indices) {
                names.add(teams.results[i]?.name.toString())
            }
            names
        } else
            null
    }
    }

    fun getSubjectId(subjectPosition: Int): Int? {
        return if (!subjects?.value?.results.isNullOrEmpty())
            subjects?.value?.results?.get(subjectPosition)?.id
        else
            null
    }

    fun getDocumentCategoryId(position: Int): Int? {
        return documentsCategories?.value?.results?.get(position)?.id
    }

    fun getDocuments(map: HashMap<String, String>): MutableLiveData<Documents>? {
        documents = repository.getDocuments(map)
        return documents
    }

    fun getUser(): MutableLiveData<User>? {
        return if (user == null) {
            user = repository.getUser()
            user
        } else
            user
    }

}
