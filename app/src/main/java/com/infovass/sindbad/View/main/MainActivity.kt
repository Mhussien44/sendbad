package com.infovass.sindbad.View.main

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.navigation.NavigationView
import com.infovass.sindbad.IOnBackPressed
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.Utils.USERID
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.AboutScreen.AboutFragment
import com.infovass.sindbad.View.ContactUs.ContactFragment
import com.infovass.sindbad.View.LoginScreen.LoginFragment
import com.infovass.sindbad.View.LoginScreen.StartupFragment
import com.infovass.sindbad.View.MessageDetails.MessageDetailsFragment
import com.infovass.sindbad.View.MessagesScreen.MessagesFragment
import com.infovass.sindbad.View.NotificationsScreen.NotificationsFragment
import com.infovass.sindbad.View.ProfileScreen.ProfileFragment
import com.infovass.sindbad.View.news.NewsFragment
import com.infovass.sindbad.data.model.MessageResultsItem
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.app_bar_main2.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        initBarIcons(drawerLayout)
        navigateToTargetScreen()
        socialMediaIconsSetup()
    }

    private fun initBarIcons(drawerLayout: DrawerLayout) {
        appMenu.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }

        barNotification.setOnClickListener {
            supportFragmentManager.inTransaction {
                replace(R.id.mainContainer, NotificationsFragment.newInstance()).addToBackStack("NotificationsFragment")
            }
        }
    }

    private fun socialMediaIconsSetup() {
        val viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getSocialMedia().observe(this, Observer { links ->
            if (links != null) {
                facebook.setOnClickListener {
                    val url = links.socialMediaResults?.facebook
                    openLink(url)

                }
                youtube.setOnClickListener {
                    val url = links.socialMediaResults?.youtube
                    openLink(url)

                }
                instagtam.setOnClickListener {
                    val url = links.socialMediaResults?.instegram
                    openLink(url)

                }
            }
        })
    }

    private fun openLink(url: String?) {
        if (url != null) {
            if (url.contains("http")) {
                val openUrlIntent = Intent(ACTION_VIEW, Uri.parse(url))
                startActivity(openUrlIntent)
            }
        }

    }

    private fun navigateToTargetScreen() {
        val token = SharedPreferenceUtil(this).getString(TOKEN, "")
        val isFromNotification = intent?.extras?.getBoolean("fromNotification", false)
        if (token == "")
            supportFragmentManager.inTransaction {
                add(R.id.mainContainer, StartupFragment.newInstance())
            }
        else {
            if (isFromNotification == true) {
                val message = intent?.extras?.getParcelable<MessageResultsItem>("MessageResultsItem")
                supportFragmentManager.inTransaction {
                    add(
                        R.id.mainContainer,
                        MessageDetailsFragment.newInstance(message, true)
                    )
                }
            } else
                supportFragmentManager.inTransaction { add(R.id.mainContainer, MainFragment.newInstance()) }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                supportFragmentManager?.inTransaction {
                    replace(R.id.mainContainer, MainFragment.newInstance())
                }
            }
            R.id.nav_message , R.id.nav_contact-> {
                supportFragmentManager?.inTransaction {
                    replace(R.id.mainContainer, MessagesFragment.newInstance()).addToBackStack("MessagesFragment")
                }
            }
            R.id.nav_profile -> {
                supportFragmentManager?.inTransaction {
                    replace(R.id.mainContainer, ProfileFragment.newInstance()).addToBackStack("ProfileFragment")
                }
            }
            R.id.nav_logout -> {
                removeUserData()
                supportFragmentManager?.inTransaction {
                    replace(R.id.mainContainer, LoginFragment.newInstance())
                }
            }
                R.id.nav_news ->{
                    supportFragmentManager?.inTransaction {
                        replace(R.id.mainContainer, NewsFragment.newInstance())
                    }

            }
           /* R.id.nav_contact -> {
                supportFragmentManager?.inTransaction {
                    replace(R.id.mainContainer, ContactFragment.newInstance()).addToBackStack("ProfileFragment")
                }
            }*/
            R.id.nav_about -> {
                supportFragmentManager?.inTransaction {
                    replace(R.id.mainContainer, AboutFragment.newInstance()).addToBackStack("ProfileFragment")
                }
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            val fragment = this.supportFragmentManager.findFragmentById(R.id.mainContainer)
            if (fragment is IOnBackPressed) {
                (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
                    if (!it)
                        super.onBackPressed()
                }
            } else
                super.onBackPressed()
        }

    }


    private fun removeUserData() {
        SharedPreferenceUtil(this).apply {
            putString(TOKEN, "")
            putString(USERID, "")
        }
    }
}
