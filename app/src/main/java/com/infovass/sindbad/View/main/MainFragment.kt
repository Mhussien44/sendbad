package com.infovass.sindbad.View.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.IOnBackPressed
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.View.AboutScreen.AboutFragment
import com.infovass.sindbad.View.ContactUs.ContactFragment
import com.infovass.sindbad.View.Documents.DocumentsFragment
import com.infovass.sindbad.View.QRScanner.CodeScannerActivity
import com.infovass.sindbad.View.educationVideo.EducationalVideosFragment
import com.infovass.sindbad.View.news.NewsAdapter
import com.infovass.sindbad.View.news.NewsFragment
import com.infovass.sindbad.View.test.TestTypeActivity
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() , IOnBackPressed {

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initAppBar()

        activity?.appBarTitle?.text = resources.getString(R.string.app_name)

        mainVideoQr.setOnClickListener {
            startActivity(Intent(context!!, CodeScannerActivity::class.java))
        }

        edu_video.setOnClickListener {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer , EducationalVideosFragment.newInstance()).addToBackStack("EducationalVideosFragment")
            }
        }

        mainSubjects.setOnClickListener {
            fragmentManager?.inTransaction {
                replace(R.id.mainContainer , DocumentsFragment.newInstance()).addToBackStack("DocumentsFragment")
            }
        }

        mainMcq.setOnClickListener {
            val intent = Intent(context, TestTypeActivity::class.java)
            intent.putExtra("screen_type", 1)
            startActivity(intent)
        }
    }

    private fun initAppBar() {
        activity?.appBarTitle?.text = resources.getString(R.string.app_name)
        activity?.appMenu?.visibility = View.VISIBLE
        activity?.barNotification?.visibility = View.VISIBLE
    }

    override fun onBackPressed(): Boolean {
        fragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        return true
    }
}
