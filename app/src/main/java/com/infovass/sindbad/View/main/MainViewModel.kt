package com.infovass.sindbad.View.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.SocialMedia
import com.infovass.sindbad.data.repository.AppRepository

class MainViewModel (application: Application) : AndroidViewModel(application) {

    val token = SharedPreferenceUtil(getApplication()).getString(TOKEN,"")
    val repository = AppRepository(token.toString())

    fun getSocialMedia(): MutableLiveData<SocialMedia> {
        return repository.getSocialMedia()
    }
}
