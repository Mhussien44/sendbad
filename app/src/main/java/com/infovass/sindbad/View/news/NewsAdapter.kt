package com.infovass.sindbad.View.news

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.inTransaction
import com.infovass.sindbad.data.model.NewsResultsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.notification_item.view.*

class NewsAdapter(
    context: Context,
    private var messages: List<NewsResultsModel>,
    private val fragmentManager: FragmentManager?
) :
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.notification_item, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = messages.get(position)
        holder.bind(data, position)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return if (messages.isNullOrEmpty())
            0
        else
            messages.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(data: NewsResultsModel?, position: Int) {
            itemView.notificationTitle.text = data?.title
            itemView.notificationDesc.text = data?.subDescription
            itemView.notificationDate.text = data?.createdAt

            Picasso.get().load(data?.media).into(itemView.messageIcon)

            itemView.setOnClickListener {
                fragmentManager?.inTransaction {
                    replace(R.id.mainContainer , NewsDetailsFragment.newInstance(data)).addToBackStack("DocumentsFragment")
                }
            }
        }
    }
}