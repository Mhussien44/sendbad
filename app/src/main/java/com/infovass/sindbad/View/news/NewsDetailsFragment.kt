package com.infovass.sindbad.View.news

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infovass.sindbad.R
import com.infovass.sindbad.data.model.NewsResultsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_news_details.*

class NewsDetailsFragment : Fragment() {

    companion object{
        fun newInstance(data: NewsResultsModel?) = NewsDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable("NewsDetails" , data)
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_details, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val newsDetails = arguments?.getParcelable<NewsResultsModel>("NewsDetails")
        newsTitle.text = newsDetails?.title
        newsDescription.text = newsDetails?.description
        newsDate.text = newsDetails?.createdAt

        Picasso.get().load(newsDetails?.media).into(newsImg)

    }


}
