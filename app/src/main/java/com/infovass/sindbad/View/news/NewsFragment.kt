package com.infovass.sindbad.View.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.data.model.NewsResultsModel
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.news_fragment.*

class NewsFragment : Fragment() {

    companion object {
        fun newInstance() = NewsFragment()
    }

    private lateinit var viewModel: NewsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        activity?.appBarTitle?.text = resources.getString(R.string.news)

        newsList.layoutManager = LinearLayoutManager(context)
        val progress = CustomProgressBar.showProgressBar(context!!)
        progress.show()
        viewModel.getNews().observe(this, Observer { news ->
            progress.dismiss()
            if (news != null) {
                if (!news.results.isNullOrEmpty()) {
                    noNews.visibility = View.GONE
                    newsList.visibility = View.VISIBLE
                    newsList.adapter = NewsAdapter(
                        context!!,
                        news.results as List<NewsResultsModel>,
                        fragmentManager
                    )
                } else {
                    noNews.visibility = View.VISIBLE
                    newsList.visibility = View.GONE
                }
            } else {
                noNews.visibility = View.VISIBLE
                newsList.visibility = View.GONE
                makeToast(context!!, resources.getString(R.string.errors))
            }
        })
    }

}
