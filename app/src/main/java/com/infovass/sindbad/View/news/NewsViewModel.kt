package com.infovass.sindbad.View.news

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.NewsModel
import com.infovass.sindbad.data.repository.AppRepository

class NewsViewModel(application: Application) : AndroidViewModel(application) {

    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")

    private val repository = AppRepository(token.toString())

    fun getNews(): MutableLiveData<NewsModel> {
        return repository.getNews()
    }
}
