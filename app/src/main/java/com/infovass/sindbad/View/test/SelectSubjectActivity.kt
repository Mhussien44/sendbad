package com.infovass.sindbad.View.test

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import kotlinx.android.synthetic.main.activity_select_subject.*

class SelectSubjectActivity : AppCompatActivity() {

    private val testType = lazy { intent.getIntExtra("type",1) }
    private lateinit var testingViewModel: TestingViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_subject)

        testingViewModel = ViewModelProviders.of(this).get(TestingViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        progressBar.visibility = View.VISIBLE
        subjectsList.visibility = View.GONE
        testingViewModel.getSubjects()?.observe(this, Observer { subjects ->
            progressBar.visibility = View.GONE
            subjectsList.visibility = View.VISIBLE
            if (subjects != null) {
                subjectsList.layoutManager = LinearLayoutManager(this)
                subjectsList.adapter = SubjectsAdapter(this, subjects.results,testType.value)

            } else
                makeToast(this, resources.getString(R.string.errors))
        })
    }
}
