package com.infovass.sindbad.View.test

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.View.Testing.*
import kotlinx.android.synthetic.main.activity_test_type.*
import kotlinx.android.synthetic.main.alert_dialog.*

class TestTypeActivity : AppCompatActivity() {

    private val screenType = lazy { intent.getIntExtra("screen_type", 1) }
    private val testingType = lazy { intent.getIntExtra("test_type", 1) }
    private val selectedSubjectId = lazy { intent.getIntExtra("selectedSubjectId", 0) }
    private lateinit var testingViewModel: TestingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_type)
        testingViewModel = ViewModelProviders.of(this).get(TestingViewModel::class.java)

        if (screenType.value == 1) {
            titleText.text = resources.getString(R.string.mcq)
            testType.visibility = View.VISIBLE
            testChapter.visibility = View.GONE
            next.setOnClickListener {
                var type = 0
                when {
                    testing.isChecked -> {
                        type = 1
                        val intent = Intent(this, SelectSubjectActivity::class.java)
                        intent.putExtra("type", type)
                        startActivity(intent)
                    }
                    exam.isChecked -> {
                        type = 2
                        val intent = Intent(this, SelectSubjectActivity::class.java)
                        intent.putExtra("type", type)
                        startActivity(intent)
                    }
                    oneExam.isChecked -> {
                        type = 3
                        showEnterExamCodeDialog()
                    }
                }


            }
        } else if (screenType.value == 2) {
            var selectedChapter = 0
            titleText.text = resources.getString(R.string.selectChapter)
            testType.visibility = View.GONE
            testChapter.visibility = View.VISIBLE

            testChapter.setOnCheckedChangeListener { group, checkedId ->
                // find the radiobutton by returned id
                val radioButton: RadioButton = findViewById(checkedId)
                selectedChapter = getSelectedChapterIndex(radioButton.text.toString())
            }

            if (testingType.value == 1) {
                next.setOnClickListener {
                    showQuestionsCountDialog(selectedChapter, selectedSubjectId.value)
                }
            } else
                next.setOnClickListener {
                    val intent = Intent(this, TestingActivity::class.java)
                    intent.putExtra(SELECTED_CHAPTER, selectedChapter)
                    intent.putExtra(SELECTED_SUBJECT_ID, selectedSubjectId.value)
                    intent.putExtra(TESTING_TYPE, testingType.value)
                    startActivity(intent)
                }
        }
    }

    private fun getSelectedChapterIndex(chapterName: String): Int {
        return when (chapterName) {
            resources.getString(R.string.all) -> 0
            resources.getString(R.string.ch1) -> 1
            resources.getString(R.string.ch2) -> 2
            resources.getString(R.string.ch3) -> 3
            resources.getString(R.string.ch4) -> 4
            resources.getString(R.string.ch5) -> 5
            resources.getString(R.string.ch6) -> 6
            resources.getString(R.string.ch7) -> 7
            resources.getString(R.string.ch8) -> 8
            resources.getString(R.string.ch9) -> 9
            resources.getString(R.string.ch10) -> 10
            else -> 0
        }
    }

    private fun showQuestionsCountDialog(selectedChapter: Int, selectedSubjectId: Int) {
        val dialog = Dialog(this)
        val numbers = resources.getStringArray(R.array.numbers).toMutableList()
        var selectedNumber = 1

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.alert_dialog)

        dialog.alertDialogTitle.text = resources.getString(R.string.select_questions_count)
        dialog.alertDialogMesage.visibility = View.GONE
        dialog.alertDialogOk.visibility = View.VISIBLE
        dialog.alertDialogCount.visibility = View.VISIBLE

        dialog.alertDialogCount.attachDataSource(numbers)
        dialog.alertDialogCount.setOnSpinnerItemSelectedListener { parent, view, position, id ->
            selectedNumber = numbers[position].toInt()
        }
        dialog.alertDialogOk.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this, TestingActivity::class.java)
            intent.putExtra(SELECTED_CHAPTER, selectedChapter)
            intent.putExtra(SELECTED_SUBJECT_ID, selectedSubjectId)
            intent.putExtra(TESTING_TYPE, testingType.value)
            intent.putExtra(QUESTIONS_COUNT, selectedNumber)
            startActivity(intent)
        }
        dialog.alertDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    private fun showEnterExamCodeDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.alert_dialog)

        dialog.alertDialogTitle.text = resources.getString(R.string.enterTestCode)
        dialog.alertDialogMesage.visibility = View.GONE
        dialog.alertDialogCount.visibility = View.GONE
        dialog.alertDialogEditText.visibility = View.VISIBLE

        dialog.alertDialogOk.setOnClickListener {
            if (!dialog.alertDialogEditText.text.isNullOrBlank())
                getTest(dialog)
            else
                dialog.alertDialogEditText.error = resources.getString(R.string.requird)
        }
        dialog.alertDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    private fun getTest(dialog: Dialog) {
        dialog.alertDialogOk.visibility = View.GONE
        dialog.alertDialogCancel.visibility = View.INVISIBLE
        dialog.alertDialogProgress.visibility = View.VISIBLE
        testingViewModel.makeUnifiedExam(dialog.alertDialogEditText.text.toString())
            .observe(this, Observer {
                dialog.alertDialogOk.visibility = View.VISIBLE
                dialog.alertDialogCancel.visibility = View.VISIBLE
                dialog.alertDialogProgress.visibility = View.GONE
                if (it != null && it.checkErrors == false && !it.examResults.isNullOrEmpty()) {
                    dialog.dismiss()
                    val intent = Intent(this, TestingActivity::class.java)
                    intent.putExtra(TESTING_TYPE, 3)
                    intent.putExtra(UNIFIED_TESTING_DATA, it.examResults)
                    startActivity(intent)
                } else
                    makeToast(this, resources.getString(R.string.errors))
            })
    }
}
