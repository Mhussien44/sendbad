package com.infovass.sindbad.View.test

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.ExamResponseModel
import com.infovass.sindbad.data.model.Subjects
import com.infovass.sindbad.data.repository.AppRepository

class TestingViewModel(application: Application) : AndroidViewModel(application) {

    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")
    private val repository = AppRepository(token.toString())
    private var subjects: MutableLiveData<Subjects>? = null

    fun getSubjects(): MutableLiveData<Subjects>? {
        return if (subjects == null) {
            subjects = repository.getSubjects()
            subjects
        } else
            subjects

    }

    fun makeUnifiedExam(code : String): MutableLiveData<ExamResponseModel> {
      return repository.makeUnifiedExam(code)
    }
}