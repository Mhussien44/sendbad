package com.infovass.sindbad.View.videoDetails

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.MediaController
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.util.MimeTypes
import com.google.android.exoplayer2.util.Util
import com.infovass.sindbad.R
import com.infovass.sindbad.Utils.CustomProgressBar
import com.infovass.sindbad.Utils.SnackAndToastUtil.Companion.makeToast
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.model.VideoBySubjectResult
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.login_fragment.view.*
import kotlinx.android.synthetic.main.vedio_details_fragment.*
import java.io.PrintWriter
import java.io.StringWriter
import java.io.Writer


class VideosDetailsFragment : Fragment(), Player.EventListener {






        var user:User?=null



    companion object {
        fun newInstance() = VideosDetailsFragment()

        fun newInstance(videoBySubjectResult: VideoBySubjectResult?,user:User) =
            VideosDetailsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("videoBySubjectResult", videoBySubjectResult)
                    putSerializable("user", user)

                }
            }

    }


    private var viewModel: VideosDetailsViewModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.vedio_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

         if (viewModel == null)
            viewModel = ViewModelProviders.of(this).get(VideosDetailsViewModel::class.java)


        val videoBySubjectResult =
            arguments?.getSerializable("videoBySubjectResult") as VideoBySubjectResult


        user=    arguments?.getSerializable("user") as User

        if(user != null) {
            userTextView.text=user?.success!!.phone
            val rightSwipe: Animation = AnimationUtils.loadAnimation(activity, R.anim.anim_right)
            rightSwipe.repeatCount=20000
            rightSwipe.repeatMode=2
            userTextView.startAnimation(rightSwipe);
        }

        val progress = CustomProgressBar.showProgressBar(context!!)

        if(videoBySubjectResult!= null) {
            progress.show()
          //  activity?.appBarTitle?.text =  videoBySubjectResult.name
            getVideoData(progress, videoBySubjectResult)

        }


        videoView.setOnPreparedListener(object : MediaPlayer.OnPreparedListener {
            override fun onPrepared(mp: MediaPlayer?) {
                //close the progress dialog when buffering is done
                progress.dismiss()
            }
        })


        videoView.setOnErrorListener { mediaPlayer, i, i1 -> false



        }


        val controller = MediaController(activity)
        controller.setMediaPlayer(videoView)
        videoView.setMediaController(controller)
//        getUserInfo()



    }



    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) {
         }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()
        videoView.stopPlayback();

    }

    override fun onStop() {
        super.onStop()
        videoView.stopPlayback();

    }




    private fun getVideoData(progress: KProgressHUD, videoBySubjectResult: VideoBySubjectResult) {
        try {
            val map = HashMap<String, String>()
            map["videoId"] = videoBySubjectResult.id
            viewModel?.getVideosDetails(map)?.observe(this, Observer { videoDetails ->

                progress.dismiss()
                if (videoDetails != null) {
//                player.setSource(Uri.parse(videoDetails.results.link))
                    if (videoDetails.results != null) {
                        if (videoDetails.results.link != null) {
                            progress.show()
                            videoView.setVideoPath(videoDetails.results!!.link);
                            videoView.start();
//                            val rightSwipe: Animation = AnimationUtils.loadAnimation(activity, R.anim.anim_right)
//
//                            userTextView.startAnimation(rightSwipe);
                        } else {
                            makeToast(context!!, "empty video")
                        }

                    } else {
//
                    }
                } else {
//
                }
            })
        }catch (e: Exception){

            val writer: Writer = StringWriter()
            e.printStackTrace(PrintWriter(writer))
            val s: String = writer.toString()
            makeToast(context!!, s + "4")

        }
    }







}
