package com.infovass.sindbad.View.videoDetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.infovass.sindbad.Utils.SharedPreferenceUtil
import com.infovass.sindbad.Utils.TOKEN
import com.infovass.sindbad.data.model.*
import com.infovass.sindbad.data.repository.AppRepository

class VideosDetailsViewModel(application: Application) : AndroidViewModel(application) {

    private val token = SharedPreferenceUtil(getApplication()).getString(TOKEN, "")
    private val repository = AppRepository(token.toString())
    private var user: MutableLiveData<User>? = null
    private var videosDetails: MutableLiveData<VideoDetailsResponse>? = null


    fun getVideosDetails(map: HashMap<String, String>): MutableLiveData<VideoDetailsResponse>? {

            videosDetails = repository.getVideosDetails(map)
         return   videosDetails


    }


    fun getUser(): MutableLiveData<User>? {
        return if (user == null) {
            user = repository.getUser()
            user
        } else
            user
    }

}
