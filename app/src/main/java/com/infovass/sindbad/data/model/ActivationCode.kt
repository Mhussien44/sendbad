package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

class ActivationCode(
    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("check_errors")
    val checkErrors: Boolean? = null,

    @field:SerializedName("code")
    val code: String? = null
)