package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class Area(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("name")
    val name: String? = null
)