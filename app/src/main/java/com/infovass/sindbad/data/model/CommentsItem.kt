package com.infovass.sindbad.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CommentsItem @JvmOverloads constructor(

	@field:SerializedName("commentable_type")
	val commentableType: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("message_id")
	val messageId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("commentable_id")
	val commentableId: Int? = null,

	@field:SerializedName("text")
	val text: String? = null
) : Parcelable