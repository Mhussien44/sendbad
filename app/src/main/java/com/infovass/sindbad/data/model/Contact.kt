package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class Contact(

	@field:SerializedName("results")
	val resultsContact: ResultsContact? = null,

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null,

	@field:SerializedName("message")
	val message: Boolean? = null
)