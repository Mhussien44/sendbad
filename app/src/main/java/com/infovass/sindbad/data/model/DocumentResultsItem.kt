package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class DocumentResultsItem(

	@field:SerializedName("subject_id")
	val subjectId: Int? = null,

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("document_category_id")
	val documentCategoryId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("actionType")
	var actionType: String? = null
)