package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class Documents(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null,

	@field:SerializedName("results")
	val results: List<DocumentResultsItem?>? = null
)