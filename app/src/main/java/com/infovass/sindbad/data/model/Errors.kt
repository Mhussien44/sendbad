package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class Errors(

	@field:SerializedName("qrCode")
	val qrCode: List<String?>? = null,

	@field:SerializedName("phone")
	val phone: List<String?>? = null,

	@field:SerializedName("email")
	val email: List<String?>? = null

)