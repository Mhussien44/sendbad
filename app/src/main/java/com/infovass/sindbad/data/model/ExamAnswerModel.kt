package com.infovass.sindbad.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExamAnswerModel(

	@field:SerializedName("selectedAnswer")
	var selectedAnswer: Int? = null,

	@field:SerializedName("right_answer")
	val rightAnswer: Int? = null,

	@field:SerializedName("answer3")
	val answer3: String? = null,

	@field:SerializedName("answer2")
	val answer2: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("answer4")
	val answer4: String? = null,

	@field:SerializedName("question_id")
	val questionId: Int? = null,

	@field:SerializedName("answer1")
	val answer1: String? = null,

	@field:SerializedName("picture")
	val picture: String? = null
) : Parcelable