package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class ExamResponseModel(

	@field:SerializedName("results")
	val examResults: ArrayList<ExamResultsModel?>? = null,

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null
)