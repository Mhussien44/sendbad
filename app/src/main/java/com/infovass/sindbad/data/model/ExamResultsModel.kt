package com.infovass.sindbad.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExamResultsModel(

	@field:SerializedName("subject_id")
	val subjectId: Int? = null,

	@field:SerializedName("chapter")
	val chapter: Int? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("public_exam_question")
	val publicExamQuestion: List<PublicExamQuestionModel?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("time")
	val time: String? = null
) : Parcelable