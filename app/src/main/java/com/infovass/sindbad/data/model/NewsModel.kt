package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class NewsModel(

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null,

	@field:SerializedName("results")
	val results: List<NewsResultsModel?>? = null
)