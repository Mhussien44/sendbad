package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class NotificationItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("owner_id")
	val ownerId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("owner_type")
	val ownerType: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)