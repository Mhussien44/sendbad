package com.infovass.sindbad.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PublicExamQuestionModel(

	@field:SerializedName("public_exam_id")
	val publicExamId: Int? = null,

	@field:SerializedName("answer")
	val examAnswer: ExamAnswerModel? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("question")
	val examQuestion: ExamQuestionModel? = null,

	@field:SerializedName("question_id")
	val questionId: Int? = null,

	@field:SerializedName("answer_id")
	val answerId: Int? = null
) : Parcelable