package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName
import com.infovass.sindbad.data.repository.RegistrationData

data class RegistrationDataModel(

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null,

	@field:SerializedName("results")
	val results: RegistrationData? = null
)