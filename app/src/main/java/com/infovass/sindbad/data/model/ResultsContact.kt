package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class ResultsContact(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("student_id")
	val studentId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
)