package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class SendMessageResponse(
    @field:SerializedName("check_errors")
    val checkErrors: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("results")
    val results: CommentsItem? = null
)