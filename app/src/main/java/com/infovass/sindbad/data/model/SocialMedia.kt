package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class SocialMedia(

	@field:SerializedName("results")
	val socialMediaResults: SocialMediaResults? = null,

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null
)