package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class SocialMediaResults(

	@field:SerializedName("youtube")
	val youtube: String? = null,

	@field:SerializedName("student_term")
	val studentTerm: Int? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("instegram")
	val instegram: String? = null,

	@field:SerializedName("admin_term")
	val adminTerm: Int? = null
)