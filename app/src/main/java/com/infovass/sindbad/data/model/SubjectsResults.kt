package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class SubjectsResults(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("team_id")
	val teamId: Int? = null
)