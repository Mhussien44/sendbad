package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName

data class TestingResponseModel(

	@field:SerializedName("results")
	val testinresults: List<TestinresultsModel?>? = null,

	@field:SerializedName("check_errors")
	val checkErrors: Boolean? = null
)