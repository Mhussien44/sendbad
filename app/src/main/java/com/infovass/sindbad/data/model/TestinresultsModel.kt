package com.infovass.sindbad.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TestinresultsModel(

	@field:SerializedName("subject_id")
	val subjectId: Int? = null,

	@field:SerializedName("chapter")
	val chapter: Int? = null,

	@field:SerializedName("answer")
	val answer: AnswerModel? = null,

	@field:SerializedName("difficult")
	val difficult: Int? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("term")
	val term: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("team_id")
	val teamId: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("picture")
	val picture: String? = null
) : Parcelable