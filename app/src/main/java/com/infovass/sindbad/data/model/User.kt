package com.infovass.sindbad.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User (
    @field:SerializedName("accessToken")
    val accessToken: String? = null,

    @field:SerializedName("success")
    val success: Success? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("errors")
    val errors: Errors? = null,

    @field:SerializedName("check_errors")
    val checkErrors: Boolean? = null,

    @field:SerializedName("results")
    val result: Success? = null
):Serializable