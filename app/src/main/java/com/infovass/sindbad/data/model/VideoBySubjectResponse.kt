package com.infovass.sindbad.data.model

import java.io.Serializable

data class VideoBySubjectResponse(
    val check_errors: Boolean,
    val results: List<VideoBySubjectResult>
):Serializable