package com.infovass.sindbad.data.model

import java.io.Serializable

data class VideoDetailsResponse(
    val check_errors: Boolean,
    val results: VideoDetailsResults? = null,
    val message:String? = null

):Serializable