package com.infovass.sindbad.data.model

import java.io.Serializable

data class VideoDetailsResults(
    val activated: Boolean,
    val id: String,
    val link: String,
    val name: String,
    val subject_id: Int,
    val watch_count: Int
):Serializable