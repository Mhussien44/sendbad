package com.infovass.sindbad.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.infovass.sindbad.Utils.enqueue
import com.infovass.sindbad.data.model.*
import com.infovass.sindbad.data.service.ApiService
import com.infovass.sindbad.data.service.ServiceGenerator

class  AppRepository(token : String) {

    var apiService: ApiService? = null

    init {
        apiService = ServiceGenerator(token).createService
    }

    fun verifyPhone(code: String): MutableLiveData<User> {
        val result = MutableLiveData<User>()
        val call = apiService?.verifyPhone(code)
        call?.enqueue {
            onResponse = { response ->
                Log.i("verifyPhone", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("verifyPhone/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, User::class.java)
                        result.value = errorResponse
                        Log.e("verifyPhone/body", errors)
                    }
                    else -> {
                        Log.e("verifyPhone/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("verifyPhone", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun validateCodeScanner(qrResult: String): MutableLiveData<QrScanner> {
        val result = MutableLiveData<QrScanner>()
        val call = apiService?.validateQrCode(qrResult)
        call?.enqueue {
            onResponse = { response ->
                Log.i("validateCodeScanner", response.code().toString())
                Log.i("validateCodeScanner/url", call.request().url().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("validateCode/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, QrScanner::class.java)
                        result.value = errorResponse
                        Log.i("validateCode/body", errors)
                    }
                    else -> {
                        Log.i("validateCode/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("validateCodeScanner", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getTeams(): MutableLiveData<RegistrationDataModel> {
        val result = MutableLiveData<RegistrationDataModel>()
        val call = apiService?.getTeams()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getTeams", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getTeams/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getTeams/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getTeams", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getGroupsOfTeam(teamId : Int): MutableLiveData<SingleTeam> {
        val result = MutableLiveData<SingleTeam>()
        val call = apiService?.getGroupsOfTeam(teamId)
        call?.enqueue {
            onResponse = { response ->
                Log.i("getGroupsOfTeam", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getGroupsOfTeam/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getGroupsOfTeam/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getGroupsOfTeam", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getSubjects(): MutableLiveData<Subjects> {
        val result = MutableLiveData<Subjects>()
        val call = apiService?.getStudentSubjects()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getSubjects", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getSubjects/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getSubjects/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getSubjects", t!!.message)
                result.value = null
            }
        }
        return result
    }



    fun getEducationalVideos(map: HashMap<String,String>): MutableLiveData<VideoBySubjectResponse> {
        val result = MutableLiveData<VideoBySubjectResponse>()
        val call = apiService?.videoBySubject(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("getEducationalVideos", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("EducationalVideos/body", response.body().toString())

                    }
                    else -> {
                        Log.i("EducationalVideos/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getSubjects", t!!.message)
                result.value = null
            }
        }
        return result
    }


    fun getVideosDetails(map: HashMap<String,String>): MutableLiveData<VideoDetailsResponse> {
        val result = MutableLiveData<VideoDetailsResponse>()
        val call = apiService?.getVideo(map)
        call?.enqueue {
            onResponse = { response ->
                 Log.i("getEducationalVideos", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("EducationalVideos/body", response.body().toString())

                    }
                    else -> {
                        Log.i("EducationalVideos/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getSubjects", t!!.message)
                result.value = null
            }
        }
        return result
    }



    fun getDocumentCategories(): MutableLiveData<DocumentCategories> {
        val result = MutableLiveData<DocumentCategories>()
        val call = apiService?.getDocumentCategories()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getDocumentCategories", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getCategories/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getCategories/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getSubjects", t!!.message)
                result.value = null
            }
        }
        return result
    }


    fun getDocuments(map: HashMap<String,String>): MutableLiveData<Documents> {
        val result = MutableLiveData<Documents>()
        val call = apiService?.getDocuments(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("getDocuments/url", call.request().url().toString())
                Log.i("getDocuments", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getDocuments/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, Documents::class.java)
                        result.value = errorResponse
                        Log.i("getDocuments/body", errors)
                    }
                    else -> {
                        Log.i("getDocuments/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getDocuments", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getUser(): MutableLiveData<User> {
        val result = MutableLiveData<User>()
        val call = apiService?.getUser()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getUser", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getUser/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, User::class.java)
                        result.value = errorResponse
                        Log.i("getUser/body", errors)
                    }
                    else -> {
                        Log.i("getUser/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getUser", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getAbout(): MutableLiveData<About> {
        val result = MutableLiveData<About>()
        val call = apiService?.getAbout("about")
        call?.enqueue {
            onResponse = { response ->
                Log.i("getAbout", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getAbout/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getAbout/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getAbout", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getSocialMedia(): MutableLiveData<SocialMedia> {
        val result = MutableLiveData<SocialMedia>()
        val call = apiService?.getSocialMedia()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getSocialMedia", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getSocialMedia/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getSocialMedia/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getSocialMedia", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getNotifications(): MutableLiveData<Notification> {
        val result = MutableLiveData<Notification>()
        val call = apiService?.getNotifications()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getNotifications", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getNotifications/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getNotifications/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getNotifications", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun getNews(): MutableLiveData<NewsModel> {
        val result = MutableLiveData<NewsModel>()
        val call = apiService?.getNews()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getNews", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getNews/body", response.body().toString())

                    }
                    else -> {
                        Log.i("getNews/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getNews", t!!.message)
                result.value = null
            }
        }
        return result
    }

     fun makeTesting(map: HashMap<String, String>): MutableLiveData<List<TestinresultsModel?>> {
        val result = MutableLiveData<List<TestinresultsModel?>>()
        val call = apiService?.makeTesting(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("makeTesting/url", call.request().url().toString())
                Log.i("makeTesting/code", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body().testinresults
                        Log.i("makeTesting/body", response.body().toString())

                    }
                    else -> {
                        Log.i("makeTesting/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("makeTesting", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun makeUnifiedExam(code : String): MutableLiveData<ExamResponseModel> {
        val result = MutableLiveData<ExamResponseModel>()
        val call = apiService?.makeUnifiedExam(code)
        call?.enqueue {
            onResponse = { response ->
                Log.i("makeUnifiedExam", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("makeUnifiedExam/body", response.body().toString())

                    }
                    else -> {
                        Log.i("makeUnifiedExam/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("makeUnifiedExam", t!!.message)
                result.value = null
            }
        }
        return result
    }
}