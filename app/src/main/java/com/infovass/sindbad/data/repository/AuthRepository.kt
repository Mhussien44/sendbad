package com.infovass.sindbad.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.infovass.sindbad.Utils.enqueue
import com.infovass.sindbad.data.model.ActivationCode
import com.infovass.sindbad.data.model.ChangePassResponse
import com.infovass.sindbad.data.model.User
import com.infovass.sindbad.data.service.ApiService
import com.infovass.sindbad.data.service.ServiceGenerator

class AuthRepository(token : String) {

    var apiService: ApiService? = null

    init {
        apiService = ServiceGenerator(token).createService
    }

    fun register(map: HashMap<String,String>): MutableLiveData<User> {
        val result = MutableLiveData<User>()
        val call = apiService?.register(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("register", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("register/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, User::class.java)
                        result.value = errorResponse
                        Log.e("register/errors", errors)
                    }
                    else -> {
                        Log.e("register/errors", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("register/onFailure", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun login(map: HashMap<String,String>): MutableLiveData<User> {
        val result = MutableLiveData<User>()
        val call = apiService?.login(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("login", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("login/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, User::class.java)
                        result.value = errorResponse
                        Log.e("login/body/errors", errors)
                    }
                    else -> {
                        Log.e("login/bodyerrors", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("login/onFailure", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun resetPassword(map: HashMap<String,String>): MutableLiveData<User> {
        val result = MutableLiveData<User>()
        val call = apiService?.resetPassword(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("resetPassword", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("resetPassword/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, User::class.java)
                        result.value = errorResponse
                        Log.e("resetPass/body/errors", errors)
                    }
                    else -> {
                        Log.e("resetPass/body/errors", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("resetPassword/onFailure", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun resetPassRequest(phone : String): MutableLiveData<User> {
        val result = MutableLiveData<User>()
        val call = apiService?.resetPassRequest(phone)
        call?.enqueue {
            onResponse = { response ->
                Log.i("resetPassRequest", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("resetPassRequest/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, User::class.java)
                        result.value = errorResponse
                        Log.e("resetRequest/body/error", errors)
                    }
                    else -> {
                        Log.e("resetRequest/body/error", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("resetRequest/onFailure", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun changePassword(map: HashMap<String,String>): MutableLiveData<ChangePassResponse> {
        val result = MutableLiveData<ChangePassResponse>()
        val call = apiService?.changePass(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("changePassword", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("changePassword/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, ChangePassResponse::class.java)
                        result.value = errorResponse
                        Log.e("changePass/body/error", errors)
                    }
                    else -> {
                        Log.e("changePass/body/error", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("changePass/onFailure", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun resendActivationCode(phone: String): MutableLiveData<ActivationCode> {
        val result = MutableLiveData<ActivationCode>()
        val call = apiService?.resendActivationCode(phone)
        call?.enqueue {
            onResponse = { response ->
                Log.i("resendCode", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("resendCode/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, ActivationCode::class.java)
                        result.value = errorResponse
                        Log.e("resendCode/body/error", errors)
                    }
                    else -> {
                        Log.e("resendCode/body/error", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("resendCode/onFailure", t!!.message)
                result.value = null
            }
        }
        return result
    }
}