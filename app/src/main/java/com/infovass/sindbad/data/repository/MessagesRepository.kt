package com.infovass.sindbad.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.infovass.sindbad.Utils.enqueue
import com.infovass.sindbad.data.model.Contact
import com.infovass.sindbad.data.model.Messages
import com.infovass.sindbad.data.model.SendMessageResponse
import com.infovass.sindbad.data.service.ApiService
import com.infovass.sindbad.data.service.ServiceGenerator

class MessagesRepository(token: String) {

    var apiService: ApiService? = null

    init {
        apiService = ServiceGenerator(token).createService
    }

    fun getMessages(): MutableLiveData<Messages> {
        val result = MutableLiveData<Messages>()
        val call = apiService?.getMyMessages()
        call?.enqueue {
            onResponse = { response ->
                Log.i("getMessages", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("getMessages/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, Messages::class.java)
                        result.value = errorResponse
                        Log.i("getMessages/body", errors)
                    }
                    else -> {
                        Log.i("getMessages/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("getMessages", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun sendContactRequest(map: HashMap<String, String>): MutableLiveData<Contact> {
        val result = MutableLiveData<Contact>()
        val call = apiService?.sendContactRequest(map)
        call?.enqueue {
            onResponse = { response ->
                Log.i("sendContactRequest", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("sendContactRequest/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, Contact::class.java)
                        result.value = errorResponse
                        Log.i("sendContactRequest/body", errors)
                    }
                    else -> {
                        Log.i("sendContactRequest/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("sendContactRequest", t!!.message)
                result.value = null
            }
        }
        return result
    }

    fun messageReply(messageID: Int, text: String): MutableLiveData<SendMessageResponse> {
        val result = MutableLiveData<SendMessageResponse>()
        val call = apiService?.messageReply(messageID, text)
        call?.enqueue {
            onResponse = { response ->
                Log.i("messageReply", response.code().toString())
                when (response.code() / 100) {
                    2 -> {
                        result.value = response.body()
                        Log.i("messageReply/body", response.body().toString())

                    }
                    4 -> {
                        val errors = response.errorBody()?.string()
                        val gson = Gson()
                        val errorResponse =
                            gson.fromJson(errors, SendMessageResponse::class.java)
                        result.value = errorResponse
                        Log.i("messageReply/body", errors)
                    }
                    else -> {
                        Log.i("messageReply/body", response.errorBody().string())
                        result.value = null
                    }
                }
            }
            onFailure = { t ->
                Log.e("messageReply", t!!.message)
                result.value = null
            }
        }
        return result
    }
}