package com.infovass.sindbad.data.repository

import com.google.gson.annotations.SerializedName
import com.infovass.sindbad.data.model.Area
import com.infovass.sindbad.data.model.TeamItem

data class RegistrationData(

    @field:SerializedName("areas")
    val areas: List<Area?>? = null,

    @field:SerializedName("teams")
    val teams: List<TeamItem?>? = null
)