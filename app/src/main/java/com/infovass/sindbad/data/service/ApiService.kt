package com.infovass.sindbad.data.service

import com.infovass.sindbad.data.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    companion object {
        const val BASEURL = "https://sindbad-edu.com/"
    }

    @Headers("Accept: application/json")
    @POST("/api/register")
    fun register(@QueryMap map: HashMap<String, String>): Call<User>

    @Headers("Accept: application/json")
    @POST("/api/login")
    fun login(@QueryMap map: HashMap<String, String>): Call<User>

    @Headers("Accept: application/json")
    @POST("/api/resetPassword")
    fun resetPassword(@QueryMap map: HashMap<String, String>): Call<User>

    @Headers("Accept: application/json")
    @POST("/api/resendSMS")
    fun resendActivationCode(@Query("phone") phone: String): Call<ActivationCode>

    @Headers("Accept: application/json")
    @POST("/api/reset")
    fun resetPassRequest(@Query("phone") phone: String): Call<User>

    @Headers("Accept: application/json")
    @POST("/api/changePassword")
    fun changePass(@QueryMap map: HashMap<String, String>): Call<ChangePassResponse>



    @Headers("Accept: application/json")
    @POST("/api/videoBySubject")
    fun videoBySubject(@QueryMap map: HashMap<String, String>): Call<VideoBySubjectResponse>



    @Headers("Accept: application/json")
    @POST("/api/getVideo")
    fun getVideo(@QueryMap map: HashMap<String, String>): Call<VideoDetailsResponse>



    @Headers("Accept: application/json")
    @POST("/api/getUser")
    fun getUser(): Call<User>

    //Auth Apis
    @Headers("Accept: application/json")
    @POST("/api/video")
    fun validateQrCode(
        @Query("qrCode") qrCode: String
    ): Call<QrScanner>

    @Headers("Accept: application/json")
    @POST("/api/registrationData")
    fun getTeams(): Call<RegistrationDataModel>

    @Headers("Accept: application/json")
    @POST("/api/getTeamGroups/{id}")
    fun getGroupsOfTeam(@Path("id") teamId: Int): Call<SingleTeam>

    @Headers("Accept: application/json")
    @POST("/api/groupStudentSubjects")
    fun getStudentSubjects(): Call<Subjects>
    @Headers("Accept: application/json")

    @POST("/api/documentCategories")
    fun getDocumentCategories(): Call<DocumentCategories>

    @Headers("Accept: application/json")
    @POST("/api/documents")
    fun getDocuments(@QueryMap map: HashMap<String, String>): Call<Documents>

    @Headers("Accept: application/json")
    @POST("/api/myMessages")
    fun getMyMessages(): Call<Messages>

    @Headers("Accept: application/json")
    @POST("/api/phoneVerify")
    fun verifyPhone(
        @Query("phone_code") code: String
    ): Call<User>

    @Headers("Accept: application/json")
    @POST("/api/message/create")
    fun sendContactRequest(@QueryMap map: HashMap<String, String>): Call<Contact>

    @Headers("Accept: application/json")
    @POST("/api/testing")
    fun makeTesting(@QueryMap map: HashMap<String, String>): Call<TestingResponseModel>

    @Headers("Accept: application/json")
    @POST("/api/publicExam")
    fun makeUnifiedExam(@Query("code") code: String): Call<ExamResponseModel>

    @Headers("Accept: application/json")
    @POST("/api/comment/{message_id}/create")
    fun messageReply(
        @Path("message_id") messageId: Int,
        @Query("text") text: String
    ): Call<SendMessageResponse>

    @Headers("Accept: application/json")
    @POST("/api/settings")
    fun getAbout(@Query("key") key:String): Call<About>

    @Headers("Accept: application/json")
    @POST("/api/settings")
    fun getSocialMedia(): Call<SocialMedia>

    @Headers("Accept: application/json")
    @POST("/api/myNotifications")
    fun getNotifications(): Call<Notification>

    @Headers("Accept: application/json")
    @POST("/api/news")
    fun getNews(): Call<NewsModel>
}