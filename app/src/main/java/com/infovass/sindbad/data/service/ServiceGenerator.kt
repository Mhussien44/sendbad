package com.infovass.sindbad.data.service


import com.google.gson.GsonBuilder
import com.infovass.sindbad.data.service.ApiService.Companion.BASEURL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ServiceGenerator(token: String) {



    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor { chain ->
            var request = chain.request()
            var httpUrl = request.url()

            httpUrl = httpUrl.newBuilder()
                .addQueryParameter("", "")
                .build()
            request = request.newBuilder().url(httpUrl)
                .addHeader("Authorization" , "Bearer $token").build()
            chain.proceed(request)
        }.build()
    var gson = GsonBuilder()
        .setLenient()
        .create()
    private val builder = Retrofit.Builder()
        .baseUrl(BASEURL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))

    private val retrofit = builder.build()

    val createService = retrofit.create(ApiService::class.java)
}